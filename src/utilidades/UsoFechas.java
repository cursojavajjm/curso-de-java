package utilidades;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class UsoFechas {
	
	public static final long MILISEGUNDOS_AL_DIA = 24 * 60 * 60 * 1000;
	public static final long MILISEGUNDOS_AL_ANYO = MILISEGUNDOS_AL_DIA * 365;
	
	public long diferenciaEntreFechas(Date d1, Date d2) {
		long t1 = d1.getTime();
		long t2 = d2.getTime();
		return (t1-t2)/MILISEGUNDOS_AL_ANYO;
	}
	
	public static void main(String[] args) {
		Date hoy = new Date(); 
		Calendar calendar = new GregorianCalendar(2015, 8, 23);  // Meses van entre 0 y 11: 0 es Enero
		Date haceUnA�o = new Date(calendar.getTimeInMillis());
		
		UsoFechas fechas = new UsoFechas();
		System.out.println("Tiempo; "+ fechas.diferenciaEntreFechas(hoy, haceUnA�o));
	}

}
