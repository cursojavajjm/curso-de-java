package utilidades;

public class GeneradorDatos {

	public static enum Tipo {
		NOMBRE_FEMENINO, NOMBRE_MASCULINO, APELLIDO, GENERO_PELICULA, PELICULA
	};

	public static final String[] NOMBRES_FEMENINOS = { "Alba", "Alicia", "Ana", "Andrea", "Beatriz", "Bel�n", "Blanca",
			"B�rbara", "Camila", "Carmen", "Carolina", "Claudia", "Cristina", "Diana", "Elena", "Elizabeth", "Emily",
			"Emma", "Erika", "Esther", "Fabiola", "Fernanda", "F�tima", "Gabriela", "Gloria", "Guadalupe", "G�nesis",
			"Isabel", "Jennifer", "Jessica", "Karen", "Karina", "Karla", "Katherine", "Kiara", "Laura", "Lorena",
			"Luc�a", "Luna", "L�a", "Martina", "Mar�a", "Miriam", "M�nica", "Natalia", "Nicole", "Noah", "Pamela",
			"Paola", "Patricia", "Paula", "Paulina", "Roc�o", "Rosa", "Sandra", "Sara", "Silvia", "Sof�a", "Susana",
			"Tania", "Valentina", "Valeria", "Vanessa", "Ver�nica", "Victoria", "Ximena", "Yesenia", "Zoe" };

	public static final String[] NOMBRES_MASCULINOS = { "Adri�n", "Alberto", "Alejandro", "Andr�s", "Antonio",
			"Benjamin", "Bruno", "Bryan", "Carlos", "Cristian", "Daniel", "David", "Diego", "Dylan", "Eduardo",
			"Enrique", "Fabi�n", "Felipe", "Fernando", "Francisco", "Gabriel", "Gael", "Guillermo", "Gustavo", "Hugo",
			"H�ctor", "Ian", "Ignacio o Nacho", "Iker", "Isaac", "Iv�n", "Javier", "Jes�s", "Joel", "Jonathan", "Jorge",
			"Jos�", "Juan", "Kevin", "Lucas", "Luis", "Manuel", "Marcos", "Mart�n", "Mateo", "Miguel", "Nacho",
			"Nicol�s", "Omar", "Pablo", "Pedro", "Rafael", "Ricardo", "Roberto", "Rodrigo", "Samuel", "Santiago",
			"Sebasti�n", "Sergio", "Thiago", "Uriel", "V�ctor", "�lvaro", "�scar" };

	public static final String[] APELLIDOS = { "GARCIA", "GONZALEZ", "RODRIGUEZ", "FERNANDEZ", "LOPEZ", "MARTINEZ",
			"SANCHEZ", "PEREZ", "GOMEZ", "MARTIN", "JIMENEZ", "RUIZ", "HERNANDEZ", "DIAZ", "MORENO", "ALVAREZ", "MU�OZ",
			"ROMERO", "ALONSO", "GUTIERREZ", "NAVARRO", "TORRES", "DOMINGUEZ", "VAZQUEZ", "RAMOS", "GIL", "RAMIREZ",
			"SERRANO", "BLANCO", "SUAREZ", "MOLINA", "MORALES", "ORTEGA", "DELGADO", "CASTRO", "ORTIZ", "RUBIO",
			"MARIN", "SANZ", "NU�EZ", "IGLESIAS", "MEDINA", "GARRIDO", "SANTOS", "CASTILLO", "CORTES", "LOZANO",
			"GUERRERO", "CANO", "PRIETO", "MENDEZ", "CALVO", "CRUZ", "GALLEGO", "VIDAL", "LEON", "HERRERA", "MARQUEZ",
			"PE�A", "CABRERA", "FLORES", "CAMPOS", "VEGA", "DIEZ", "FUENTES", "CARRASCO", "CABALLERO", "NIETO", "REYES",
			"AGUILAR", "PASCUAL", "HERRERO", "SANTANA", "LORENZO", "HIDALGO", "MONTERO", "IBA�EZ", "GIMENEZ", "FERRER",
			"DURAN", "VICENTE", "BENITEZ", "MORA", "SANTIAGO", "ARIAS", "VARGAS", "CARMONA", "CRESPO", "ROMAN",
			"PASTOR", "SOTO", "SAEZ", "VELASCO", "SOLER", "MOYA", "ESTEBAN", "PARRA", "BRAVO", "GALLARDO", "ROJAS" };

	public static final String[] GENEROS_PELICULAS = { "Cine adolescente", "Cine arte", "Cine b�lico",
			"Cine de g�nsteres", "Cine cat�strofe", "Cine costumbrista", "Cine cristiano", "Cine de acci�n",
			"Cine de animaci�n", "Cine de artes marciales", "Cine de autor", "Cine de aventuras",
			"Cine de ciencia ficci�n", "Cine de samur�is", "Cine de terror", "Cine documental", "Cine �pico",
			"Cine experimental", "Cine fant�stico", "Cine gore", "Cine hist�rico", "Cine musical", "Cine negro",
			"Cine polic�aco", "Cine pol�tico", "Cine pornogr�fico", "Cine propagand�stico", "Cine rom�ntico", "Cine S",
			"Cine surrealista", "Clase B", "Comedia", "Drama", "Melodrama", "Thriller", "Western", "Cine underground",
			"Dramedia", "Road movie" };

	public static final String[] PELICULAS = { " CIUDADANO KANE", " CASABLANCA (1942)", " EL PADRINO",
			" LO QUE EL VIENTO SE LLEV�", " LAWRENCE DE ARABIA", " EL MAGO DE OZ", " EL GRADUADO",
			" LA LEY DEL SILENCIO", " LA LISTA DE SCHINDLER", " CANTANDO BAJO LA LLUVIA", " �QU� BELLO ES VIVIR!",
			" EL CREP�SCULO DE LOS DIOSES", " EL PUENTE SOBRE EL R�O KWAI", " CON FALDAS Y A LO LOCO",
			" LA GUERRA DE LAS GALAXIAS", " EVA AL DESNUDO", " LA REINA DE �FRICA", " PSICOSIS", " CHINATOWN (1974)",
			" ALGUIEN VOL� SOBRE EL NIDO DEL CUCO", " LA UVAS DE LA IRA", " 2001: UNA ODISEA DEL ESPACIO",
			" EL HALC�N MALT�S", " TORO SALVAJE", " E", " TEL�FONO ROJO: VOLAMOS HACIA MOSC�",
			" BONNIE AND CLYDE (1967)", " APOCALYPSE NOW (1979)", " CABALLERO SIN ESPADA", " EL TESORO DE SIERRA MADRE",
			" ANNIE HALL (1977)", " EL PADRINO PARTE II", " SOLO ANTE EL PELIGRO", " MATAR A UN RUISE�OR",
			" SUCEDI� UNA NOCHE", " COWBOY DE MEDIANOCHE", " LOS MEJORES A�OS DE NUESTRAS VIDAS", " PERDICI�N",
			" DOCTOR ZHIVAGO (1965)", " CON LA MUERTE EN LOS TALONES", " WEST SIDE STORY (1961)",
			" LA VENTANA INDISCRETA", " KING KONG (1933)", " EL NACIMIENTO DE UNA NACI�N", " UN TRANV�A LLAMADO DESEO",
			" LA NARANJA MEC�NICA", " TAXI DRIVER (1976)", " TIBUR�N", " BLANCANIEVES Y LOS SIETE ENANITOS",
			" DOS HOMBRES Y UN DESTINO", " HISTORIAS DE PHILADELPHIA", " DE AQU� A LA ETERNIDAD", " AMADEUS (1984)",
			" SIN NOVEDAD EN EL FRENTE", " SONRISAS Y L�GRIMAS", " M*A*S*H (1970)", " EL TERCER HOMBRE",
			" FANTASIA (1940)", " REBELDE SIN CAUSA", " EN BUSCA DEL ARCA PERDIDA", " V�RTIGO (DE ENTRE LOS MUERTOS)",
			" TOOTSIE (1982)", " LA DILIGENCIA", " ENCUENTROS EN LA TERCERA FASE", " EL SILENCIO DE LOS CORDEROS",
			" NETWORK (UN MUNDO IMPLACABLE)", " EL MENSAJERO DEL MIEDO", " UN AMERICANO EN PARIS", " RA�CES PROFUNDAS",
			" CONTRA EL IMPERIO DE LA DROGA", " FORREST GUMP (1994)", " BEN-HUR (1959)", " CUMBRES BORRASCOSAS",
			" LA QUIMERA DEL ORO", " BAILANDO CON LOBOS", " LUCES DE CIUDAD", " AMERICAN GRAFFITI (1973)",
			" ROCKY (1976)", " EL CAZADOR", " GRUPO SALVAJE", " TIEMPOS MODERNOS", " GIGANTE", " PLATOON (1986)",
			" FARGO (1996)", " SOPA DE GANSO", " MOT�N A BORDO", " FRANKENSTEIN (1931)", " BUSCANDO MI DESTINO",
			" PATTON (1970)", " EL CANTOR DE JAZZ", " MY FAIR LADY (1964)", " UN LUGAR EN EL SOL", " EL APARTAMENTO",
			" UNO DE LOS NUESTROS", " PULP FICTION (1994)", " CENTAUROS DEL DESIERTO", " LA FIERA DE MI NI�A",
			" SIN PERD�N", " ADIVINA QUIEN VIENE ESTA NOCHE", " YANQUI DANDY", };

	public static String generar(Tipo dato) {
		int i;
		String valor = null;
		switch (dato) {
		case NOMBRE_FEMENINO:
			i = (int) (Math.random() * (NOMBRES_FEMENINOS.length));
			valor = NOMBRES_FEMENINOS[i];
			break;
		case NOMBRE_MASCULINO:
			i = (int) (Math.random() * (NOMBRES_MASCULINOS.length));
			valor = NOMBRES_MASCULINOS[i];
			break;
		case APELLIDO:
			i = (int) (Math.random() * (APELLIDOS.length));
			valor = APELLIDOS[i];
			break;
		case GENERO_PELICULA:
			i = (int) (Math.random() * (GENEROS_PELICULAS.length));
			valor = GENEROS_PELICULAS[i];
			break;
		case PELICULA:
			i = (int) (Math.random() * (PELICULAS.length));
			valor = PELICULAS[i];
			break;
		default:
			break;
		}
		return valor;
	}

}
