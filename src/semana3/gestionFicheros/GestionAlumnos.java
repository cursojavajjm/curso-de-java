package semana3.gestionFicheros;

import java.util.HashSet;
import java.util.Set;

public class GestionAlumnos {
	
	private Set<Alumno> alumnos;
	private IGestionFicheros gestor;
	
	public GestionAlumnos(IGestionFicheros gestor) {
		this.gestor = gestor;
		this.alumnos = new HashSet<Alumno>();
	}
	
	public void cargarDatos(String direccion) {
		this.alumnos = this.gestor.cargarAlumnos(direccion);
	}
	
	public void guardarDatos(String direccion) {
		this.gestor.guardarAlumnos(direccion, this.alumnos);
	}
	
	public boolean darAlta(Alumno a) {
		return this.alumnos.add(a);
	}

	public boolean darBaja(Alumno a) {
		return this.alumnos.remove(a);
	}
	
	public void mostrar() {
		for(Alumno a : this.alumnos) {
			System.out.println(a.toString());
		}
	}
	
}
