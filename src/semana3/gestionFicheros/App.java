package semana3.gestionFicheros;

import semana3.gestionFicheros.impl.GestionFicherosScanner;

public class App {
	
	public static void main(String[] args) {
//		IGestionFicheros gestor = new GestionFicherosBufferedReader();
		IGestionFicheros gestor = new GestionFicherosScanner();
		GestionAlumnos app = new GestionAlumnos(gestor);
		app.cargarDatos("C:\\Users\\NewUser\\git\\curso-de-java\\src\\semana3\\gestionFicheros\\alumnos.txt");
		app.mostrar();
		app.guardarDatos("C:\\Users\\NewUser\\git\\curso-de-java\\src\\semana3\\gestionFicheros\\alumnos_copia.txt");
	}

}
