package semana3.gestionFicheros;

import java.util.Set;

public interface IGestionFicheros {

	Set<Alumno> cargarAlumnos(String direccion);
	void guardarAlumnos(String direccion, Set<Alumno> alumnos);
	
}
