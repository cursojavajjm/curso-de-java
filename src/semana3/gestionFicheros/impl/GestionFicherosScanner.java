package semana3.gestionFicheros.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import semana3.gestionFicheros.Alumno;
import semana3.gestionFicheros.IGestionFicheros;

public class GestionFicherosScanner implements IGestionFicheros {

	@Override
	public Set<Alumno> cargarAlumnos(String direccion) {
		Set<Alumno> resultado = new HashSet<Alumno>();
		Scanner sc = null;
		try {
			File f = new File(direccion);
			sc = new Scanner(f);
			sc.useDelimiter(";");
			
			while(sc.hasNextLine()) {
				int numero = sc.nextInt();
				String nombre = sc.next();
				String apellido = sc.next();
				int edad = sc.nextInt();
				
				Alumno a = new Alumno(numero, nombre, apellido, edad);
				resultado.add(a);
				
				sc.nextLine();
			}
			
			return resultado;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sc != null)
				sc.close();
		}
		return null;
	}

	@Override
	public void guardarAlumnos(String direccion, Set<Alumno> alumnos) {
		FileWriter fw = null;
		StringBuilder sb = new StringBuilder();
		try {
			File f = new File(direccion);
			fw = new FileWriter(f);
			for(Alumno a : alumnos) {
				sb.append(a.toFileString());
				sb.append("\n");
			}
			fw.write(sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fw != null)
				try {
					fw.close();
				} catch (IOException e) { /**/ }
		}
	}

}
