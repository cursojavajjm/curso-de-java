package semana3.interfacesGraficos;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class EjemploTablas extends JFrame {
	
	private JTable tabla;
	private JButton btn1;
	private JButton btn2;
	
	public EjemploTablas() {
		super();
		setTitle("Ejemplo de Tablas");
		setBounds(150, 100, 600, 400);
		
		
		addControls();
		
		setVisible(true);
	}
	
	private void addControls() {
		getContentPane().setLayout(new GridLayout(2, 1));
		
		// Fila 1
		DefaultTableModel modelo = new DefaultTableModel();
		modelo.setColumnIdentifiers(new String[]{"Nombre", "Edad"});
		modelo.addRow(new String[]{"Nombre1", "14"});
		modelo.addRow(new String[]{"Nombre2", "24"});
		modelo.addRow(new String[]{"Nombre3", "27"});
		modelo.addRow(new String[]{"Nombre4", "15"});
		this.tabla = new JTable(modelo);
		this.tabla.setVisible(true);
		JScrollPane fila1 = new JScrollPane(this.tabla);
		fila1.setVisible(true);
		getContentPane().add(fila1);
		
		// Fila 2
		JPanel fila2 = new JPanel();
		fila2.setLayout(new GridLayout(1, 2));
		this.btn1 = new JButton("Cargar datos");
		this.btn1.setVisible(true);
		fila2.add(this.btn1);
		this.btn2 = new JButton("Borrar datos");
		this.btn2.setVisible(true);
		fila2.add(this.btn2);
		fila2.setVisible(true);
		getContentPane().add(fila2);
		
	}
	
	public static void main(String[] args) {
		EjemploTablas tb = new EjemploTablas();
	}

}
