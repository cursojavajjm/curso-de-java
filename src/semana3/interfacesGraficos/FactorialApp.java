package semana3.interfacesGraficos;

import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class FactorialApp extends Frame {
	
	private Button calcularBtn;
	private Label tituloLbl;
	private TextField cifraText;
	private TextField resultadoText;
	
	public FactorialApp() {
		this("Calculo Factorial", 150, 80, 1200, 800);
	}
	
	public FactorialApp(String titulo, int x, int y, int w, int h) {
		setTitle(titulo);
		setBounds(x, y, w, h);
		setVisible(true);
		setLayout(null);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				FactorialApp.this.dispose();
			}
		});
		addControls();
	}
	
	private void addControls() {
		// Cifra
		this.cifraText = new TextField();
		this.cifraText.setBounds(600, 150, 150, 100);
		this.cifraText.setEditable(true);
		this.add(this.cifraText);
		
		// Label
		this.tituloLbl = new Label("Factorial: ");
		this.tituloLbl.setBounds(400, 300, 150, 100);
		this.add(this.tituloLbl);
		
		// Resultado
		this.resultadoText = new TextField();
		this.resultadoText.setBounds(600, 300, 150, 100);
		this.resultadoText.setEditable(false);
		this.add(this.resultadoText);
		
		// Boton
		this.calcularBtn = new Button("Calcular");
		this.calcularBtn.setBounds(600, 500, 150, 100);
		ActionListener escuchador = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int num = Integer.valueOf(FactorialApp.this.cifraText.getText());
				int factorial = factorial(num);
				FactorialApp.this.resultadoText.setText(String.valueOf(factorial));
			}
		};
		this.calcularBtn.addActionListener(escuchador);
		this.add(this.calcularBtn);
	}
	
	private int factorial(int n) {
		int res = 1;
		for(int i=n; i>0; i--) {
			res *= i;
		}
		return res;
	}

	
	public static void main(String[] args) {
		FactorialApp app = new FactorialApp();
	}
	
	
}
