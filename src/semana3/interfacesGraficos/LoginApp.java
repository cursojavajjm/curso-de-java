package semana3.interfacesGraficos;

import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;

public class LoginApp extends Frame {
	
	private HashMap<String, String> credenciales;
	
	private TextField userTxt;
	private TextField pwdTxt;
	private Button loginBtn;
	private Label msgLbl;
	
	public LoginApp() {
		this("LoginApp", 150, 80, 600, 500);
	}
	
	public LoginApp(String titulo, int x, int y, int w, int h) {
		setTitle(titulo);
		setBounds(x, y, w, h);
		setVisible(true);
		setLayout(null);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				LoginApp.this.dispose();
			}
		});
		
		this.credenciales = cargarCredenciales();
		
		addControls();
	}
	
	private HashMap<String, String> cargarCredenciales() {
		HashMap<String, String> cred = new HashMap<String, String>();
		cred.put("hola@ejemplo.com", "1234");
		cred.put("cursojava", "asdf");
		return cred;
	}
	
	private void addControls() {
		int ancho = 150;
		int alto = 50;
		
		Label userLbl = new Label("Usuario:");
		userLbl.setBounds(20, 100, 100, alto);
		this.add(userLbl);
		
		this.userTxt = new TextField();
		this.userTxt.setBounds(140, 100, ancho, alto);
		this.userTxt.setEditable(true);
		this.add(this.userTxt);
		
		Label pwdLbl = new Label("Password:");
		pwdLbl.setBounds(20, 190, 100, alto);
		this.add(pwdLbl);
		
		this.pwdTxt = new TextField();
		this.pwdTxt.setBounds(140, 190, ancho, alto);
		this.pwdTxt.setEditable(true);
//		this.pwdTxt.setEchoChar('*');
		this.pwdTxt.addKeyListener(new KeyAdapter() {
//			@Override
//			public void keyTyped(KeyEvent e) {
//				char c = e.getKeyChar();
//				if (c >= '0' && c <= '9') {
//					System.out.println("numeros");
//				} else {
//					e.consume();
//				}
//			}
			@Override
			public void keyPressed(KeyEvent e) {
				char c = e.getKeyChar();
				if (!(c >= '0' && c <= '9')) {
					e.consume();
				}
			}
		});
		this.add(this.pwdTxt);
		
		this.loginBtn = new Button("Login");
		this.loginBtn.setBounds(140, 290, ancho, alto);
		this.loginBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String usuario = LoginApp.this.userTxt.getText();
				String password = LoginApp.this.pwdTxt.getText();
				if (usuario.isEmpty() || password.isEmpty()) {
					LoginApp.this.msgLbl.setText("Por favor introduzca el usuario y contraseņa");
				} else {
					if (LoginApp.this.credenciales.containsKey(usuario)) {
						String pwdRecuperada = LoginApp.this.credenciales.get(usuario);
						if (pwdRecuperada.equals(password))
							 LoginApp.this.msgLbl.setText("Login! ");
						else LoginApp.this.msgLbl.setText("Contraseņa incorrecta");
					} else {
						LoginApp.this.msgLbl.setText("El usuario no existe");
					}
				}
				System.out.println(LoginApp.this.pwdTxt.getText());
			}
		});
		this.add(this.loginBtn);
		
		this.msgLbl = new Label("");
		this.msgLbl.setBounds(50, 360, 300, 50);
		this.add(this.msgLbl);
		
	}
	
	public static void main(String[] args) {
		LoginApp app = new LoginApp();
	}

}
