package semana3.interfacesGraficos.swing;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class EjemploSwing extends JFrame {
	
	private int ancho, alto;
	
	private JLabel lbl;
	private JButton btn;
	
	private JTable tabla;
	
	public EjemploSwing() {
		super();
		setTitle("Ejemplo con SWING");
		ancho = 600;
		alto = 400;
		setBounds(150, 100, ancho, alto);
		
		int numFilas = 2;
		int numColumnas = 1;
		getContentPane().setLayout(new GridLayout(numFilas, numColumnas));
		addControls();
		setVisible(true);
	}
	
	private void addControls() {
		
		// Fila 1
		DefaultTableModel modelo = new DefaultTableModel();
		modelo.setColumnIdentifiers(new Object[]{"Nombre", "Edad"});
		modelo.addRow(new Object[]{"Jose", "30"});
		modelo.addRow(new Object[]{"Jordi", "29"});
		modelo.addRow(new Object[]{"Lucia", "28"});
		
		this.tabla = new JTable(modelo);
		this.tabla.setVisible(true);
		JScrollPane fila1 = new JScrollPane(this.tabla);
		fila1.setVisible(true);
		this.getContentPane().add(fila1);

		// Fila 2
		JPanel fila2 = new JPanel();
		fila2.setLayout(new GridLayout(1, 2));
		
		ImageIcon icon = new ImageIcon("imagenes/icono.gif");
		this.lbl = new JLabel("Etiqueta", icon, (int) JComponent.CENTER_ALIGNMENT);
		this.lbl.setBounds(20, 420, 150, 50);
		fila2.add(this.lbl);
		
		this.btn = new JButton("Click");
		this.btn.setBounds(170, 120, 150, 50);
		this.btn.setVisible(true);
		this.btn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Click!!");
			}
		});
		fila2.add(this.btn);
		
		this.getContentPane().add(fila2);
	}
	
	public static void main(String[] args) {
		EjemploSwing ej = new EjemploSwing();
	}
	

}
