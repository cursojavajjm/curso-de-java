package semana3.interfacesGraficos;

import java.awt.Button;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.TextArea;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MiVentana extends Frame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5919089346284296174L;
	
	private Button boton;
	private TextField texto1;
	private TextArea texto2;

	public MiVentana() {
		this("Curso Java", 150, 80, 1200, 800);
	}
	
	public MiVentana(String titulo, int x, int y, int w, int h) {
		setTitle(titulo);
		setBounds(x, y, w, h);
		setVisible(true);
		setLayout(null);
		
		addControls();
	}
	
	private void addControls() {
		// Boton
		this.boton = new Button("IMPRIMIR");
		this.boton.setBounds(100, 100, 300, 100);
		this.boton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Has pulsado el boton");
				MiVentana.this.imprimir();
			}
		});
		this.add(this.boton);
		
		// TextField
		this.texto1 = new TextField();
		this.texto1.setBounds(450, 100, 400, 100);
		this.texto1.setEditable(true);
		this.texto1.setText("Esto es un ejemplo");
		this.add(this.texto1);
		
		// TextArea
		this.texto2 = new TextArea();
		this.texto2.setBounds(900, 100, 300, 400);
		this.texto2.setEditable(true);
		this.add(this.texto2);
	}
	
	public void imprimir() {
		String textAImprimir = this.texto1.getText();
		System.out.println("Imprimiendo... "+ textAImprimir);
	}
	
	@Override
	public void paint(Graphics g) {
		int x = this.getSize().width / 2;
		int y = this.getSize().height / 2;
		
		// Arriba izquierda
		g.drawRect(20, 20, 300, 200);
		
		// Arriba derecha
		g.drawRect(this.getSize().width - 320, 20, 300, 200);
		
		// Abajo izquierda
		g.drawRect(20, this.getSize().height - 220, 300, 200);
		
		// Abajo derecha
		g.drawRect(this.getSize().width - 320, this.getSize().height - 220, 300, 200);
		
	}

}
