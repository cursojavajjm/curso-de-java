package semana3.ficheros;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class ManejoFicherosScanner {
	
	public List<Double> calcularMedia(String fichero) {
		List<Double> notas = new ArrayList<Double>();
		Scanner s = null;
		try {
			File f = new File(fichero);
			s = new Scanner(f);
			s.useDelimiter(";");
			s.useLocale(Locale.US);
			
			while(s.hasNextDouble()) {
				double nota = s.nextDouble();
				notas.add(nota);
			}
			
			return notas;
		} catch (FileNotFoundException e) {
			System.out.println("El fichero "+ fichero +" no existe");
		} catch (IOException e) {
			System.out.println("Ha habido un error");
			e.printStackTrace();
		} finally {
			if (s != null)
				s.close();
		}
		return null;
	}
	
	
	public double media(List<Double> notas) {
		double suma = 0.0;
		for(Double nota : notas) {
			suma += nota;
		}
		return suma / notas.size();
	}

	public static void main(String[] args) {
		ManejoFicherosScanner app = new ManejoFicherosScanner();
		
		// �� En direccion poned la de de vuestro fichero !!
		String direccion = "C:\\Users\\NewUser\\git\\curso-de-java\\src\\semana3\\ficheros\\notas.txt";
		List<Double> notas = app.calcularMedia(direccion);
		if (notas != null) {
			System.out.println("Notas: "+ Arrays.toString(notas.toArray()));
			double media = app.media(notas);
			System.out.println("Media: "+ media);
		}
	}

}
