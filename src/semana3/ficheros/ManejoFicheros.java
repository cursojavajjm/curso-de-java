package semana3.ficheros;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import utilidades.GeneradorDatos;
import utilidades.GeneradorDatos.Tipo;

public class ManejoFicheros {
	
	public ManejoFicheros() {
	}
	
	public String leerLinea(String fichero) {
		BufferedReader br = null;
		try {
			FileReader reader = new FileReader(fichero);
			br = new BufferedReader(reader);
			
			String linea;
			while((linea = br.readLine()) != null) {
				
			}
			return linea;
		} catch (FileNotFoundException e) {
			System.out.println("El fichero "+ fichero +" no existe");
		} catch (IOException e) {
			System.out.println("Ha habido un error");
			e.printStackTrace();
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) { /**/ }
		}
		return null;
	}
	
	public void escribirNombres(String fichero) {
		FileWriter fw = null;
		StringBuilder sb = new StringBuilder();
		String nombre = "";
		String apellido1 = "";
		String apellido2 = "";
		try {
			fw = new FileWriter(fichero, true);
			for (int i=0; i<6; i++) {
				if ((i % 2) == 0)
					 nombre = GeneradorDatos.generar(Tipo.NOMBRE_MASCULINO);
				else nombre = GeneradorDatos.generar(Tipo.NOMBRE_FEMENINO);
				apellido1 = GeneradorDatos.generar(Tipo.APELLIDO);
				apellido2 = GeneradorDatos.generar(Tipo.APELLIDO);
				sb.append(nombre);
				sb.append(";");
				sb.append(apellido1);
				sb.append(";");
				sb.append(apellido2);
				sb.append("\n");
			}
			fw.write(sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fw != null)
				try {
					fw.close();
				} catch (IOException e) { /**/ }
		}
	}
	
	public List<Double> obtenerNotas(String linea) {
		List<Double> nums = new ArrayList<Double>();
		if (linea.contains(" ")) {
			String[] numsStr = linea.split(" ");
			for (int i = 0; i < numsStr.length; i++) {
				nums.add(Double.parseDouble(numsStr[i]));
			}
		}
		return nums;
	}
	
	public double media(List<Double> notas) {
		double suma = 0.0;
		for(Double nota : notas) {
			suma += nota;
		}
		return suma / notas.size();
	}

	public static void main(String[] args) {
		ManejoFicheros app = new ManejoFicheros();
		
		// �� En direccion poned la de de vuestro fichero !!
//		String direccion = "C:\\Users\\NewUser\\git\\curso-de-java\\src\\semana3\\ficheros\\notas.txt";
//		String notasStr = app.leerLinea(direccion);
//		if (notasStr != null) {
//			System.out.println("Notas: "+ notasStr);
//			
//			List<Double> notas = app.obtenerNotas(notasStr);
//			double media = app.media(notas);
//			System.out.println("Media: "+ media);
//		}
		
		String direccion = "C:\\Users\\NewUser\\git\\curso-de-java\\src\\semana3\\ficheros\\nombres.txt";
		app.escribirNombres(direccion);
	}

}
