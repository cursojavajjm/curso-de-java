package semana3.parking.tests;

import semana3.parking.Parking;

public class Tests {

	public static void main(String[] args) {
		Parking app = new Parking();
		
		app.verAparcados();
		app.registraEntrada("12345");
		
		app.verAparcados();
		app.darAltaOficial("99999");
		app.registraEntrada("99999");
		
		app.verAparcados();
		app.registraSalida("12345");
		app.verAparcados();
		
		app.darAltaResidente("R23333");
		app.registraEntrada("R23333");
		float pago = app.registraSalida("R23333");
		System.out.println("Paga " + pago);
		
		app.generarPagosResidentes("C:\\Users\\NewUser\\git\\curso-de-java\\src\\semana3\\parking\\tests\\pagos.txt");
	}

}
