package semana3.parking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Vehiculo {
	
	private String matricula;
	private List<Estancia> aparcamientosAlMes;
	
	
	public Vehiculo(String matricula) {
		this.matricula = matricula;
		this.aparcamientosAlMes = new ArrayList<Estancia>();
	}
	
	public String getMatricula() {
		return this.matricula;
	}
	
	public void resetear() {
		this.aparcamientosAlMes.clear();
	}
	
	public boolean nuevoAparcamiento(Estancia e) {
		return this.aparcamientosAlMes.add(e);
	}

	@Override
	public String toString() {
		return "Vehiculo [matricula=" + matricula + ", aparcamientosAlMes="
				+ Arrays.toString(this.aparcamientosAlMes.toArray()) + "]";
	}
	
	

}
