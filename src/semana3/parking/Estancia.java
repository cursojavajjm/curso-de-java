package semana3.parking;

import java.util.Calendar;
import java.util.Date;

public class Estancia {
	
	private Date horaEntrada;
	private Date horaSalida;
	
	public Estancia() {
		this.horaEntrada = new Date();
		/**
		 * Simulo una fecha anterior: ���SOLO PARA SIMULAR, NO USAR ESTO!!!
		 */
		int minutos = this.horaEntrada.getMinutes();
		if (minutos > 12)
			minutos -= 10;
		if (minutos > 5)
			minutos -= 3;
		this.horaEntrada.setMinutes(minutos);
	}
	
	public void finalizarEstancia() {
		this.horaSalida = new Date();
	}
	
	public int getDuracionEstancia() {
		if (this.horaSalida != null) {
			long entrada = this.horaEntrada.getTime();
			long salida = this.horaSalida.getTime();
			long diff = salida - entrada;
			int minutos = (int) (diff / 60000); // 60 * 1000 
			return minutos;
		}
		return -1;
	}
	
	public Date getHoraEntrada() {
		return horaEntrada;
	}

	public void setHoraEntrada(Date horaEntrada) {
		this.horaEntrada = horaEntrada;
	}

	public Date getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(Date horaSalida) {
		this.horaSalida = horaSalida;
	}

	@Override
	public String toString() {
		return "horaEntrada=" + horaEntrada + ", horaSalida=" + horaSalida;
	}
	

}
