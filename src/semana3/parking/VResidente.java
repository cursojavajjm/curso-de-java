package semana3.parking;

public class VResidente extends Vehiculo {
	
	private int minutosAcumulados;

	public VResidente(String matricula) {
		super(matricula);
		this.minutosAcumulados = 0;
	}
	
	public int getMinutosAcumulados() {
		return this.minutosAcumulados;
	}
	
	public void acumularMinutos(int minutos) {
		this.minutosAcumulados += minutos;
	}
	
	@Override
	public void resetear() {
		this.minutosAcumulados = 0;
	}

}
