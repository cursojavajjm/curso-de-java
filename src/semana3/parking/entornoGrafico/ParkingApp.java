package semana3.parking.entornoGrafico;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import semana3.parking.Estancia;
import semana3.parking.Parking;

/**
 * Entorno Gr�fico para la aplicaci�n Parking
 * @author Jos� Mar�a
 *
 */
public class ParkingApp extends JFrame {
	
	private Parking parking;
	
	private JButton btnRegistrarEntrada;
	private JButton btnRegistrarSalida;
	private JButton btnAltaOficial;
	private JButton btnAltaResidente;
	private JButton btnComienzaMes;
	private JButton btnPagoResidentes;
	
	private JTable vAparcados;
	
	public ParkingApp() {
		super("ParkingApp");
		this.parking =  new Parking();
		inicializarGUI();
		setVisible(true);
	}
	
	private void inicializarGUI() {
		setBounds(500, 250, 600, 400);
		setLayout(new GridLayout(2, 1));
		
		// Fila 1: Tabla de aparcados
		DefaultTableModel modelo = crearModeloTabla();
		this.vAparcados = new JTable(modelo);
		JScrollPane fila1 = new JScrollPane(vAparcados);
		getContentPane().add(fila1);
		
		// File 2: Botones
		JPanel fila2 = new JPanel();
		fila2.setLayout(new GridLayout(2, 3));
		
		this.btnRegistrarEntrada = new JButton("Registrar Entrada");
		this.btnRegistrarEntrada.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		fila2.add(this.btnRegistrarEntrada);
		
		this.btnRegistrarSalida = new JButton("Registrar Salida");
		this.btnRegistrarSalida.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		fila2.add(this.btnRegistrarSalida);
		
		this.btnAltaOficial = new JButton("Alta Coche Oficial");
		this.btnAltaOficial.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		fila2.add(this.btnAltaOficial);
		
		this.btnAltaResidente = new JButton("Alta Coche Residente");
		this.btnAltaResidente.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		fila2.add(this.btnAltaResidente);
		
		this.btnComienzaMes = new JButton("Comienza Mes");
		this.btnComienzaMes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		fila2.add(this.btnComienzaMes);
		
		this.btnPagoResidentes = new JButton("Pago Residentes");
		this.btnPagoResidentes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		fila2.add(this.btnPagoResidentes);
		
		fila2.setVisible(true);
		getContentPane().add(fila2);
	}

	/**
	 * Crear el modelo de la tabla y rellena las filas
	 * de coches aparcados
	 * @return
	 */
	private DefaultTableModel crearModeloTabla() {
		DefaultTableModel modelo = new DefaultTableModel();
		modelo.setColumnIdentifiers(new String[]{"Matricula", "Entrada", "Salida"});
		Map<String, Estancia> aparcados = this.parking.getAparcados();
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		for(Entry<String, Estancia> aparcado : aparcados.entrySet()) {
			Estancia estancia = aparcado.getValue();
			String horaEntrada = df.format(estancia.getHoraEntrada());
			Date horaSalidaDate = estancia.getHoraSalida();
			String horaSalida = "";
			if (horaSalidaDate != null)
				horaSalida = df.format(horaSalidaDate);
			String[] fila = {
					aparcado.getKey(),
					horaEntrada,
					horaSalida
			};
			modelo.addRow(fila);
		}
		return modelo;
	}

}
