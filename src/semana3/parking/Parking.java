package semana3.parking;

import java.io.File;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Parking {
	
	private static final float PAGO_MINUTO_NORESIDENTES = (float) 0.02;
	private static final float PAGO_MINUTO_RESIDENTES = (float) 0.002;
	
	// Matricula, Vehiculo
	private Map<String, Vehiculo> oficiales;
	private Map<String, Vehiculo> residentes;
	private Map<String, Estancia> aparcados;
	
	public Parking() {
		this.oficiales = new HashMap<String, Vehiculo>();
		this.residentes = new HashMap<String, Vehiculo>();
		this.aparcados = new HashMap<String, Estancia>();
		simularDatos();
	}
	
	/**
	 * SIMULACI�N DE DATOS: ��NO USAR!!
	 */
	private void simularDatos() {
		// Dar de alta oficiales
		darAltaOficial("O9999");
		darAltaOficial("O3242");
		darAltaOficial("O9324");
		darAltaOficial("O1232");
		darAltaOficial("O9932");
		
		// Dar de alta residentes
		darAltaResidente("R23333");
		darAltaResidente("R23433");
		darAltaResidente("R43313");
		darAltaResidente("R97333");
		darAltaResidente("R39933");
		
		// Registrar Entradas
		registraEntrada("12345");
		registraEntrada("O9324");
		registraEntrada("O9932");
		registraEntrada("R43313");
		registraEntrada("12345");
		registraEntrada("09345");
		registraEntrada("86531");
		
		// Registrar Salidas
		registraSalida("O9324");
	}
	
	public void registraEntrada(String matricula) {
		Estancia aparcamiento = new Estancia();
		this.aparcados.put(matricula, aparcamiento);
		System.out.println("Vehiculo "+ matricula +" entra");
	}
	
	public float registraSalida(String matricula) {
		Estancia e = this.aparcados.get(matricula);
		e.finalizarEstancia();
		this.aparcados.remove(matricula);
		
		// Comprobar si es oficial
		Vehiculo v = this.oficiales.get(matricula);
		if (v != null) {
			v.nuevoAparcamiento(e);
			System.out.println("Vehiculo oficial "+ matricula +" sale");
			return 0;
		}
		
		// Comprobar si es residente
		v = this.residentes.get(matricula);
		if (v != null) {
			VResidente residente = (VResidente) v;
			int minutos = e.getDuracionEstancia();
			residente.acumularMinutos(minutos);
			System.out.println("Vehiculo residente "+ matricula +" sale: "+ e.toString());
			return 0;
		}
		
		// Es No Residente
		int minutosAparcados = e.getDuracionEstancia();
		float cantidadAPagar = minutosAparcados * PAGO_MINUTO_NORESIDENTES;
		System.out.println("Vehiculo no residente "+ matricula +" sale y page "+ cantidadAPagar);
		return cantidadAPagar;
	}
	
	public void darAltaOficial(String matricula) {
		Vehiculo oficial = new VOficial(matricula);
		this.oficiales.put(matricula, oficial);
	}
	
	public void darAltaResidente(String matricula) {
		Vehiculo residente = new VResidente(matricula);
		this.residentes.put(matricula, residente);
	}
	
	public void comienzaMes() {
		for (Vehiculo v : this.oficiales.values()) {
			v.resetear();
		}
		
		for (Vehiculo v : this.residentes.values()) {
			v.resetear();
		}
	}
	
	public void generarPagosResidentes(String direccion) {
		File f = null;
		FileWriter fw = null;
		StringBuilder sb = new StringBuilder();
		try {
			f = new File(direccion);
			fw = new FileWriter(f);
			for(Vehiculo v : this.residentes.values()) {
				VResidente residente = (VResidente) v;
				int minutosAcumulados = residente.getMinutosAcumulados();
				float cantidadApagar = minutosAcumulados * PAGO_MINUTO_RESIDENTES;
				sb.append(residente.getMatricula());
				sb.append("\t");
				sb.append(minutosAcumulados);
				sb.append("\t");
				sb.append(cantidadApagar);
				sb.append(System.lineSeparator());
			}
			fw.write(sb.toString());
			System.out.println("Fichero "+ f.getName() +" creado");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fw != null)
				try {
					fw.close();
				} catch (Exception e) { /**/ }
		}
	}
	
	public Map<String, Estancia> getAparcados() {
		return this.aparcados;
	}
	
	public String verAparcados() {
		StringBuilder sb = new StringBuilder();
		sb.append("Vehiculos aparcados: "+ this.aparcados.size() +"\n");
		for (Entry<String, Estancia> v : this.aparcados.entrySet()) {
			sb.append("Matricula: "+ v.getKey() +" - ");
			sb.append(v.getValue().toString() + System.lineSeparator());
		}
		System.out.println(sb.toString());
		return sb.toString();
	}

}
