package semana1;

import java.util.Hashtable;
import java.util.Map.Entry;

public class ColeccionesConClaveEjemplo {
	
	private Hashtable<String, Integer> ht = new Hashtable<String, Integer>();
	
	public void anadirElemento(String clave, Integer valor) {
		Integer valorModificado = this.ht.put(clave, valor);
		if (valorModificado != null)
			System.out.println("El valor "+ valorModificado +" con clave "+ clave +" ha sido sustituido por "+ valor);
		else System.out.println("Se ha a�adido el valor "+ valor +" con clave "+ clave);
	}
	
	public void eliminarElemento(String clave) {
		Integer valorBorrado = this.ht.remove(clave);
		if (valorBorrado != null)
			System.out.println("El valor "+ valorBorrado + " con clave "+ clave +" ha sido borrado");
	}
	
	public void mostrar() {
		System.out.println("*** HASHTABLE (No sigue orden) ***");
		for(Entry<String, Integer> e : this.ht.entrySet()) {
			System.out.println("clave: "+ e.getKey() +" - valor: "+ e.getValue());
		}
	}
	
	public static void main(String[] args) {
		ColeccionesConClaveEjemplo ejemplo = new ColeccionesConClaveEjemplo();
		ejemplo.anadirElemento("hola", 32);
		ejemplo.anadirElemento("adios", 21);
		ejemplo.anadirElemento("hola", 33);
		ejemplo.anadirElemento("fasdf", 32);
		ejemplo.mostrar();
	}

}
