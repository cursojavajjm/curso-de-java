package semana1;

public class StringUtils {
	
	
	public boolean sonIguales(String frase, String palabra1, String palabra2) {
		int numVecesPalabra1 = 0;
		int numVecesPalabra2 = 0;
		if (frase.contains(palabra1) && frase.contains(palabra2)) {
			String[] p1 = frase.split(palabra1);
			if (frase.endsWith(palabra1))
				 numVecesPalabra1 = p1.length;
			else numVecesPalabra1 = p1.length - 1;
			
			String[] p2 = frase.split(palabra2);
			if (frase.endsWith(palabra2))
				numVecesPalabra2 = p2.length;
			else numVecesPalabra2 = p2.length - 1;
			
			return (numVecesPalabra1 == numVecesPalabra2);
		} 
		return false;
	}
	

}
