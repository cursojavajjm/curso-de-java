package semana1;

public class ArrayUtils {
	
	public int[] insertar(int[] nums, int pos, int elem) {
		int[] res = new int[nums.length + 1];
		int i = 0;
		// Copiamos las posiciones 0 - pos-1
		for (i = 0; i < pos; i++) {
			res[i] = nums[i];
		}
		// Insertar el elemento elem en la posicion pos
		res[pos] = elem;
		// Copiamos de la posicion pos+1 a nums.length
		for (int j=i; j<nums.length; j++) {
			res[j+1] = nums[i];
			i++;
		}
		return res;
	}
	
	public void ordenar(int[] nums, boolean ascendente) {
		for(int i=0; i<nums.length; i++) {
			for(int j=i; j<nums.length; j++) {
				if (ascendente) {
					if (nums[j] < nums[i]) {
						int aux = nums[i];
						nums[i] = nums[j];
						nums[j] = aux;
					}
				} else {
					if (nums[j] > nums[i]) {
						int aux = nums[i];
						nums[i] = nums[j];
						nums[j] = aux;
					}
				}
			}
		}
	}
	
	public int[] ordenarParesImpares(int[] nums) {
		int[] res = new int[nums.length];
		int numPares = 0;
		int numImpares = 0;
		
		// Calcular cuantos pares e impares hay
		for(int i=0; i<nums.length; i++) {
			if (nums[i] % 2 == 0) {
				numPares++;
			} else {
				numImpares++;
			}
		}
		int[] pares = new int[numPares];
		int[] impares = new int[numImpares];
		
		// Inicializar los arrays de pares e impares
		int indicePares = 0;
		int indiceImpares = 0;
		for(int i=0; i<nums.length; i++) {
			if (nums[i] % 2 == 0) {
				pares[indicePares] = nums[i];
				indicePares++;
			} else {
				impares[indiceImpares] = nums[i];
				indiceImpares++;
			}
		}
		ordenar(pares, true);
		ordenar(impares, false);
		for(int i=0; i<numPares; i++) {
			res[i] = pares[i];
		}
		for(int i=0; i<numImpares; i++) {
			res[numPares] = impares[i];
			numPares++;
		}
		return res;
	}
	
	
}
