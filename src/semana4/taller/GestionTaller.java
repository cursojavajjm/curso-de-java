package semana4.taller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;

import semana4.taller.modelos.Cliente;
import semana4.taller.modelos.Factura;
import semana4.taller.modelos.HojaParte;
import semana4.taller.modelos.Mecanico;
import semana4.taller.modelos.Reparacion;
import semana4.taller.modelos.Repuesto;
import semana4.taller.modelos.Vehiculo;
import semana4.taller.servicios.IClienteDAO;
import semana4.taller.servicios.IFacturaDAO;
import semana4.taller.servicios.IHojaParteDAO;
import semana4.taller.servicios.IMecanicoDAO;
import semana4.taller.servicios.IReparacionDAO;
import semana4.taller.servicios.IRepuestoDAO;
import semana4.taller.servicios.IVehiculoDAO;
import semana4.taller.servicios.impl.ClienteDbDAO;
import semana4.taller.servicios.impl.FacturaDbDAO;
import semana4.taller.servicios.impl.HojaParteDbDAO;
import semana4.taller.servicios.impl.MecanicoDbDAO;
import semana4.taller.servicios.impl.ReparacionDbDAO;
import semana4.taller.servicios.impl.RepuestoDbDAO;
import semana4.taller.servicios.impl.VehiculoDbDAO;
import utilidades.GeneradorDatos;

public class GestionTaller {
	
	private static final String BD_URL = "jdbc:mysql://localhost:3306/taller";
	private static final String BD_USER = "root";
	private static final String BD_PASSWORD = "";
	
	private Connection conn;
	
	private IClienteDAO clientes;
	private IVehiculoDAO vehiculos;
	private IMecanicoDAO mecanicos;
	private IRepuestoDAO repuestos;
	private IHojaParteDAO partes;
	private IReparacionDAO reparaciones;
	private IFacturaDAO facturas;
	
	public GestionTaller() {
		this.conn = conectarDb();
		
		this.clientes = new ClienteDbDAO(this.conn);
		this.vehiculos = new VehiculoDbDAO(conn);
		this.mecanicos = new MecanicoDbDAO(conn);
		this.repuestos = new RepuestoDbDAO(conn);
		this.partes = new HojaParteDbDAO(conn);
		this.reparaciones = new ReparacionDbDAO(conn);
		this.facturas = new FacturaDbDAO(conn, partes, repuestos, vehiculos, clientes, reparaciones);
	}
	
	private Connection conectarDb() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(BD_URL, BD_USER, BD_PASSWORD);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public Factura generarFactura() {
		return null;
	}
	
	public void testearApp() {
		// Registramos vehiculo y cliente
		String dni = "13383243S";
		String nombre = GeneradorDatos.generar(GeneradorDatos.Tipo.NOMBRE_FEMENINO);
		String apellidos = GeneradorDatos.generar(GeneradorDatos.Tipo.APELLIDO);
		String direccion = GeneradorDatos.generar(GeneradorDatos.Tipo.PELICULA) + ", 14";
		int telefono = 1234567;
		Cliente cli = new Cliente(dni, nombre, apellidos, direccion, telefono);
		System.out.println(cli.toString());
		this.clientes.guardar(cli);
		
		Vehiculo v = new Vehiculo("1234ABC", "Seat", "Rojo", cli.getDni());
		System.out.println(v.toString());
		this.vehiculos.guardar(v);
		
		// Asignamos mecanico responsable y evaluacion
		Mecanico mecanicoLibre = this.mecanicos.buscarMecanicoLibre();
		System.out.println("Mecanico libre: "+ mecanicoLibre.toString());
		
		// Crear hoja parte
		HojaParte parte = new HojaParte(v.getMatricula(), 
				mecanicoLibre.getId(),
				"Esta es la evaluaci�n del mec�nico "+ mecanicoLibre.getId());
		System.out.println(parte.toString());
		
		
		// Reparamos el vehiculo: a�adir respuestos...
		List<Repuesto> repuestos = this.repuestos.buscarPorNombre("Bujia");
		int otroMecanico = 2; 
		int precioManoObra = 30;
		List<Reparacion> reparaciones = new ArrayList<Reparacion>();
		Reparacion reparacion = new Reparacion(parte.getId(), 
				otroMecanico, repuestos.get(0).getId(), 2, precioManoObra);
		reparaciones.add(reparacion);
		parte.setReparaciones(reparaciones);
		this.partes.guardar(parte);
		
		System.out.println("Reparacion: "+ reparacion.toString());
		this.partes.anadirReparacion(reparacion);
		
		// generar facturas
		Factura factura = this.facturas.generarFactura(parte.getId());
		System.out.println("Factura: "+ factura.toString());
	}
 	
	public void cerrar() {
		if (this.conn != null)
			try {
				this.conn.close();
			} catch (Exception e) { /**/ }
	}
	
	
	/**
	 * Solo para testear
	 * Este metodo no existiria en nuestro proyecto final
	 * @param args
	 */
	public static void main(String[] args) {
		GestionTaller taller = new GestionTaller();
		taller.testearApp();
		taller.cerrar();
	}
	
}
