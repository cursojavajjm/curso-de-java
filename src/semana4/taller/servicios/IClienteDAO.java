package semana4.taller.servicios;

import semana4.taller.modelos.Cliente;

public interface IClienteDAO {
	
	boolean guardar(Cliente c);
	Cliente recuperar(String dni);
	Cliente buscarPorNombre(String nombre);
	Cliente buscarPorTelefono(int telefono);
	
}
