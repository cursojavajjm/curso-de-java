package semana4.taller.servicios;

import semana4.taller.modelos.Mecanico;

public interface IMecanicoDAO {
	
	boolean guardar(Mecanico c);
	Mecanico recuperar(int id);
	
	Mecanico buscarMecanicoLibre();

}
