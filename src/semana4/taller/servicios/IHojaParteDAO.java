package semana4.taller.servicios;

import semana4.taller.modelos.HojaParte;
import semana4.taller.modelos.Reparacion;

public interface IHojaParteDAO {
	
	boolean guardar(HojaParte hoja);
	
	HojaParte recuperar(int id);
	
	boolean anadirMecanico(int idHojaParte, int idMecanico, String evaluacion);
	
	boolean anadirReparacion(Reparacion r);
	

}
