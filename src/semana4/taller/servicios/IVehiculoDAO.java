package semana4.taller.servicios;

import semana4.taller.modelos.Vehiculo;

public interface IVehiculoDAO {
	
	boolean guardar(Vehiculo v);
	Vehiculo recuperar(String matricula);

}
