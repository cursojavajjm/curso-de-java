package semana4.taller.servicios.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import semana4.taller.modelos.Cliente;
import semana4.taller.modelos.HojaParte;
import semana4.taller.modelos.Reparacion;
import semana4.taller.servicios.IHojaParteDAO;

public class HojaParteDbDAO implements IHojaParteDAO {
	
	private Connection conn;
	
	public HojaParteDbDAO(Connection conn) {
		this.conn = conn;
	}

	@Override
	public boolean guardar(HojaParte h) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date fechaEntrada = h.getFechaEntrada();
		
		
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO hoja_parte VALUES (");
		sb.append("'"+ h.getId() + "', ");
		sb.append("'"+ sdf.format(fechaEntrada) + "', ");
		sb.append("'"+ h.getMatricula() + "', ");
		sb.append("'"+ h.getMecanico() + "', ");
		sb.append("'"+ h.getEvaluacion() + "'");
		sb.append(")");
		String sql = sb.toString();
		
		Statement st = null;
		try {
			if (this.conn != null) {
				st = this.conn.createStatement();
				return st.execute(sql);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				try { st.close(); } catch (Exception e2) { /**/ }
		}
		return false;
	}

	@Override
	public HojaParte recuperar(int id) {
		String sql = "SELECT * FROM hoja_parte WHERE idHojaParte = ?";
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			if (this.conn != null) {
				st = this.conn.prepareStatement(sql);
				st.setInt(1, id);
				rs = st.executeQuery();
				if(rs.next()) {
					int idHojaParte = rs.getInt("idHojaParte");
					java.sql.Date date = rs.getDate("fechaEntrada");
					String matricula = rs.getString("vehiculo");
					int mecanicoResponsable = rs.getInt("mecanicoResponsable");
					String evaluacion = rs.getString("evaluacion");
					
					HojaParte parte = new HojaParte(idHojaParte, matricula, mecanicoResponsable, evaluacion);
					return parte;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				try { st.close(); } catch (Exception e2) { /**/ }
			if (rs != null)
				try { rs.close(); } catch (Exception e2) { /**/ }
		}
		return null;
	}

	@Override
	public boolean anadirMecanico(int idHojaParte, int idMecanico, String evaluacion) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE hoja_parte ");
		sb.append(" SET mecanicoResponsable = ?");
		sb.append(" AND evaluacion = ?");
		sb.append(" WHERE idHojaParte = ?");
		String sql = sb.toString();
		
		PreparedStatement st = null;
		try {
			if (this.conn != null) {
				st = this.conn.prepareStatement(sql);
				st.setInt(1, idMecanico);
				st.setString(2, evaluacion);
				st.setInt(3, idHojaParte);
				return st.execute(sql);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				try { st.close(); } catch (Exception e2) { /**/ }
		}
		return false;
	}

	@Override
	public boolean anadirReparacion(Reparacion r) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO reparaciones VALUES (");
		sb.append(r.getId() +", ");
		sb.append(r.getIdHojaParte() +", ");
		sb.append(r.getIdMecanico() +", ");
		sb.append(r.getIdRepuesto() +", ");
		sb.append(r.getPrecioManoObra() +", ");
		sb.append(r.getCantidad());
		sb.append(" )");
		String sql = sb.toString();
		
		PreparedStatement st = null;
		try {
			if (this.conn != null) {
				st = this.conn.prepareStatement(sql);
				return st.execute(sql);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				try { st.close(); } catch (Exception e2) { /**/ }
		}
		return false;
	}

}
