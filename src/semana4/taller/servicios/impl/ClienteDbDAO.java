package semana4.taller.servicios.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import semana4.taller.modelos.Cliente;
import semana4.taller.servicios.IClienteDAO;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class ClienteDbDAO implements IClienteDAO {
	
	private Connection conn;
	
	public ClienteDbDAO(Connection conn) {
		this.conn = conn;
	}

	@Override
	public boolean guardar(Cliente c) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO clientes VALUES (");
		sb.append("'"+ c.getDni() + "', ");
		sb.append("'"+ c.getNombre() + "', ");
		sb.append("'"+ c.getApellidos() + "', ");
		sb.append("'"+ c.getDireccion() + "', ");
		sb.append(c.getTelefono());
		sb.append(")");
		String sql = sb.toString();
		
		Statement st = null;
		try {
			if (this.conn != null) {
				st = this.conn.createStatement();
				return st.execute(sql);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				try { st.close(); } catch (Exception e2) { /**/ }
		}
		return false;
	}
	

	@Override
	public Cliente recuperar(String dni) {
		String sql = "SELECT * FROM clientes WHERE dni = ?";
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			if (this.conn != null) {
				st = this.conn.prepareStatement(sql);
				st.setString(1, dni);
				rs = st.executeQuery(sql);
				if(rs.next()) {
					String ndni = rs.getString("dni");
					String nombre = rs.getString("nombre");
					String apellidos = rs.getString("apellidos");
					String direccion = rs.getString("direccion");
					int telefono = rs.getInt("telefono");
					
					Cliente c = new Cliente(ndni, nombre, apellidos, direccion, telefono);
					return c;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				try { st.close(); } catch (Exception e2) { /**/ }
			if (rs != null)
				try { rs.close(); } catch (Exception e2) { /**/ }
		}
		return null;
	}

	@Override
	public Cliente buscarPorNombre(String nombre) {
		throw new NotImplementedException();
	}

	@Override
	public Cliente buscarPorTelefono(int telefono) {
		throw new NotImplementedException();
	}

}
