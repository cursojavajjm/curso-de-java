package semana4.taller.servicios.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import semana4.taller.modelos.Cliente;
import semana4.taller.modelos.Vehiculo;
import semana4.taller.servicios.IVehiculoDAO;

public class VehiculoDbDAO implements IVehiculoDAO {
	
	private Connection conn;
	
	public VehiculoDbDAO(Connection conn) {
		this.conn = conn;
	}

	@Override
	public boolean guardar(Vehiculo v) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO vehiculos VALUES (");
		sb.append("'"+ v.getMatricula() + "', ");
		sb.append("'"+ v.getModelo() + "', ");
		sb.append("'"+ v.getColor() + "', ");
		sb.append("'', "); // Esto es por la columna que se me ha pasado en el dise�o
		sb.append("'"+ v.getCliente() + "'");
		sb.append(")");
		String sql = sb.toString();
		
		Statement st = null;
		try {
			if (this.conn != null) {
				st = this.conn.createStatement();
				return st.execute(sql);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				try { st.close(); } catch (Exception e2) { /**/ }
		}
		return false;
	}

	@Override
	public Vehiculo recuperar(String matricula) {
		String sql = "SELECT * FROM vehiculos WHERE matricula = ?";
		
		PreparedStatement st = null;
		ResultSet rs = null;
		try {
			if (this.conn != null) {
				st = this.conn.prepareStatement(sql);
				st.setString(1, matricula);
				rs = st.executeQuery();
				if(rs.next()) {
					String nmatricula = rs.getString("matricula");
					String modelo = rs.getString("modelo");
					String color = rs.getString("color");
					String cliente = rs.getString("cliente");
					
					Vehiculo v = new Vehiculo(nmatricula, modelo, color, cliente);
					return v;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (st != null)
				try { st.close(); } catch (Exception e2) { /**/ }
			if (rs != null)
				try { rs.close(); } catch (Exception e2) { /**/ }
		}
		return null;
	}

}
