package semana4.taller.servicios.impl;

import java.sql.Connection;
import java.util.List;

import semana4.taller.modelos.Factura;
import semana4.taller.modelos.HojaParte;
import semana4.taller.modelos.Reparacion;
import semana4.taller.modelos.Repuesto;
import semana4.taller.modelos.Vehiculo;
import semana4.taller.servicios.IClienteDAO;
import semana4.taller.servicios.IFacturaDAO;
import semana4.taller.servicios.IHojaParteDAO;
import semana4.taller.servicios.IReparacionDAO;
import semana4.taller.servicios.IRepuestoDAO;
import semana4.taller.servicios.IVehiculoDAO;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class FacturaDbDAO implements IFacturaDAO {
	
	private Connection conn;
	private IHojaParteDAO partes;
	private IRepuestoDAO repuestos;
	private IVehiculoDAO vehiculos;
	private IClienteDAO clientes;
	private IReparacionDAO reparaciones;
	
	public FacturaDbDAO(Connection conn, IHojaParteDAO partes, IRepuestoDAO repuestos,
			IVehiculoDAO vehiculos, IClienteDAO clientes, IReparacionDAO reparaciones) {
		this.conn = conn;
		this.partes = partes;
		this.repuestos = repuestos;
		this.vehiculos = vehiculos;
		this.clientes = clientes;
		this.reparaciones = reparaciones;
	}

	@Override
	public boolean guardar(Factura factura) {
		throw new NotImplementedException();
	}

	@Override
	public boolean recuperar(int id) {
		throw new NotImplementedException();
	}

	@Override
	public Factura generarFactura(int idHojaParte) {
		// Recuperar objeto HojaParte
		HojaParte parte = this.partes.recuperar(idHojaParte);
		
		// Calcular precio de los respuestos y mano de obra
		int precioRepuestos = 0;
		int precioManoObra = 0;
		
		List<Reparacion> reparacionesParte = this.reparaciones.buscarPorParte(idHojaParte);
		for (Reparacion rep : reparacionesParte) {
			int idRepuesto = rep.getIdRepuesto();
			Repuesto repuesto = this.repuestos.recuperar(idRepuesto);
			int cantidad = rep.getCantidad();
			precioRepuestos += repuesto.getPrecio() * cantidad;
			precioManoObra += rep.getPrecioManoObra();
		}
		
		// Recupear objeto cliente a partir de la hora de parte: parte - matricula - cliente
		String matricula = parte.getMatricula();
		Vehiculo vehiculo = this.vehiculos.recuperar(matricula);
		
		String dniCliente = vehiculo.getCliente();
		
		// Crear factura
		Factura f = new Factura(dniCliente, idHojaParte);
		int precio = (int) ((precioRepuestos + precioManoObra) * 1.16);
		f.setPrecio(precio);
		
		return f;
	}

}
