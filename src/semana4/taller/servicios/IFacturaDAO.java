package semana4.taller.servicios;

import semana4.taller.modelos.Factura;

public interface IFacturaDAO {
	
	boolean guardar(Factura factura);
	
	boolean recuperar(int id);
	
	Factura generarFactura(int idHojaParte);

}
