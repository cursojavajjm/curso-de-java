package semana4.taller.servicios;

import java.util.List;

import semana4.taller.modelos.Reparacion;

public interface IReparacionDAO {

	boolean guardar(Reparacion r);
	Reparacion recuperar(int id);
	
	List<Reparacion> buscarPorParte(int idHojaParte);

}
