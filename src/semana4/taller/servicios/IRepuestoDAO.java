package semana4.taller.servicios;

import java.util.List;

import semana4.taller.modelos.Reparacion;
import semana4.taller.modelos.Repuesto;

public interface IRepuestoDAO {
	
	boolean guardar(Repuesto r);
	Repuesto recuperar(int id);
	
	List<Repuesto> buscarPorNombre(String nombre);

}
