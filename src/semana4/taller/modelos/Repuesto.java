package semana4.taller.modelos;

import java.util.concurrent.atomic.AtomicInteger;

public class Repuesto {
	
	private static AtomicInteger idGen = new AtomicInteger(0);
	
	private int id;
	private String nombre;
	private int precio;
	
	public Repuesto(int id, String nombre, int precio) {
		this.id = id;
		this.nombre = nombre;
		this.precio = precio;
	}
	
	public Repuesto(String nombre, int precio) {
		this.id = idGen.incrementAndGet();
		this.nombre = nombre;
		this.precio = precio;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}

	@Override
	public String toString() {
		return "Repuesto [id=" + id + ", nombre=" + nombre + ", precio=" + precio + "]";
	}
	
}
