package semana4.taller.modelos;

import java.util.concurrent.atomic.AtomicInteger;

public class Mecanico {
	
//	private static AtomicInteger idGen = new AtomicInteger(0);
	
	private int id;
	private String nombre;
	private boolean libre;
	
	public Mecanico(int id, String nombre) {
		this.id = id;
		this.nombre = nombre;
		this.libre = true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isLibre() {
		return libre;
	}

	public void setLibre(boolean libre) {
		this.libre = libre;
	}

	@Override
	public String toString() {
		return "Mecanico [id=" + id + ", nombre=" + nombre + ", libre=" + libre + "]";
	}
	
}
