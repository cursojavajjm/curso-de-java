package semana4.taller.modelos;

public class Vehiculo {
	
	private String matricula;
	private String modelo;
	private String color;
	private String cliente;
	
	public Vehiculo(String matricula, String modelo, String color, String dniCliente) {
		this.matricula = matricula;
		this.modelo = modelo;
		this.color = color;
		this.cliente = dniCliente;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	@Override
	public String toString() {
		return "Vehiculo [matricula=" + matricula + ", modelo=" + modelo + ", color=" + color + ", cliente=" + cliente
				+ "]";
	}

}
