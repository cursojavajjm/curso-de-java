package semana4.taller.modelos;

import java.util.concurrent.atomic.AtomicInteger;

public class Reparacion {
	
	private static AtomicInteger idGen = new AtomicInteger(0);
	
	private int id;
	private int idHojaParte;
	private int idMecanico;
	private int idRepuesto;
	private int cantidad;
	private int precioManoObra;
	
	public Reparacion(int idHojaParte, int idMecanico, int idRepuesto, 
			int cantidad, int precioManoObra) {
		super();
		this.id = idGen.incrementAndGet();
		this.idHojaParte = idHojaParte;
		this.idMecanico = idMecanico;
		this.idRepuesto = idRepuesto;
		this.cantidad = cantidad;
		this.precioManoObra = precioManoObra;
	}
	
	public Reparacion(int id, int idHojaParte, int idMecanico, int idRepuesto, 
			int cantidad, int precioManoObra) {
		super();
		this.id = id;
		this.idHojaParte = idHojaParte;
		this.idMecanico = idMecanico;
		this.idRepuesto = idRepuesto;
		this.cantidad = cantidad;
		this.precioManoObra = precioManoObra;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdHojaParte() {
		return idHojaParte;
	}

	public void setIdHojaParte(int idHojaParte) {
		this.idHojaParte = idHojaParte;
	}

	public int getIdMecanico() {
		return idMecanico;
	}

	public void setIdMecanico(int idMecanico) {
		this.idMecanico = idMecanico;
	}

	public int getIdRepuesto() {
		return idRepuesto;
	}

	public void setIdRepuesto(int idRepuesto) {
		this.idRepuesto = idRepuesto;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getPrecioManoObra() {
		return precioManoObra;
	}

	public void setPrecioManoObra(int precioManoObra) {
		this.precioManoObra = precioManoObra;
	}

	@Override
	public String toString() {
		return "Reparacion [id=" + id + ", idHojaParte=" + idHojaParte + ", idMecanico=" + idMecanico + ", idRepuesto="
				+ idRepuesto + ", cantidad=" + cantidad + ", precioManoObra=" + precioManoObra + "]";
	}
	
}
