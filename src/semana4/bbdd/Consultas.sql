-- SELECT * FROM northwind.orders;
USE northwind;

SELECT ShipCountry, count(*) as ProductosPorPais 
FROM northwind.orders 
GROUP BY ShipCountry
ORDER BY ProductosPorPais DESC;

SELECT AVG(UnitPrice)
FROM Products as p, Suppliers as s
WHERE p.SupplierID = s.SupplierID
AND s.Country = 'Germany';

SELECT s.Country, AVG(UnitPrice) as PrecioMedio
FROM 
	(SELECT * FROM Products) as p
	INNER JOIN Suppliers as s
	ON p.SupplierID = s.SupplierID
GROUP BY s.Country
ORDER BY PrecioMedio DESC
LIMIT 5;

SELECT s.CompanyName, sum(UnitsInStock) as Stock
FROM Products AS p, Suppliers AS s
WHERE p.SupplierId = s.SupplierId
GROUP BY s.CompanyName
ORDER BY Stock DESC;