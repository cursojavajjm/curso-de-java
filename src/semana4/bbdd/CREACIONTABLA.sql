DROP TABLE articulos;
CREATE TABLE articulos (
	codigo INT PRIMARY KEY,
    nombre VARCHAR(120),
    precio INT,
    stock INT,
    descripcion VARCHAR(600)
);

INSERT INTO articulos VALUES(1, 'Auriculares Sony', 120, 5, 'Auriculares bluetooth de la marca sony');
INSERT INTO articulos VALUES(2, 'Televisor HD.S3', 600, 15, 'Marca Samsung. Alta defincion');
INSERT INTO articulos VALUES(3, 'Televisor HASX', 120, 5, 'Tele marca Sony');
INSERT INTO articulos VALUES(4, 'Altavoces Bluetooth Bose', 60, 3, 'Altavoces inalambricos');
INSERT INTO articulos VALUES(5, 'Auriculares para iPhone', 120, 0, 'Ideal para iPhone');
INSERT INTO articulos VALUES(6, 'Portatil HDZVE', 753, 3, 'Laptop con procesador RF39S');
INSERT INTO articulos VALUES(7, 'Rat�n Inal�mbricos', 23, 5, 'Rat�n');
INSERT INTO articulos VALUES(8, 'Rat�n para PC', 10, 1, '');
INSERT INTO articulos VALUES(9, 'Reproductor DVD', 51, 2, 'Marca Philips');
INSERT INTO articulos VALUES(10, 'Auriculares para iPhone', 140, 52, 'Oficiales de Apple');
INSERT INTO articulos VALUES(11, 'Altavoz inalambrico', 53, 5, 'Resistente al agua');
INSERT INTO articulos VALUES(12, 'Tele Sony', 279, 3, 'Tevisor de alta definicion');

DROP TABLE compras;
CREATE TABLE compras (
	id INT PRIMARY KEY,
	fecha DATE,
	codArticulo INT,
    
    FOREIGN KEY fk_codigoArticulo(codArticulo)
    REFERENCES articulos(codigo)
	ON UPDATE CASCADE
    ON DELETE RESTRICT
);

