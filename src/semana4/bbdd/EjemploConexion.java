package semana4.bbdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class EjemploConexion {
	
	public void consultar(String sql, int fabricante) {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		String bdUrl = "jdbc:mysql://localhost:3306/curso_java";
		String bdUser = "root";
		String bdPass = "";
		
		Connection conn = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			conn = DriverManager.getConnection(bdUrl, bdUser, bdPass);
			if (conn != null) {
				st = conn.prepareStatement(sql);
				st.setInt(1, fabricante);
				rs = st.executeQuery(sql); // Solo para Select
				System.out.println("--- Precio medio por fabricante ---");
				while (rs.next()) {
					String codFabricante = rs.getString(1);
					String precioMedio = rs.getString(2);
					System.out.println(codFabricante +" - "+ precioMedio);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null)
				try {
					conn.close();
				} catch (SQLException e) { /**/ }
			if (st != null)
				try {
					st.close();
				} catch (SQLException e) { /**/ }
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) { /**/ }
		}
		
	}
	
	
	public static void main(String[] args) {
		EjemploConexion ej1 = new EjemploConexion();
		System.out.print("Numero de Fabricante: ");
		Scanner sc = new Scanner(System.in);
		int fabricante = sc.nextInt();
		String sql = "select fabricante, avg(precio) from articulos where fabricante = ?";
		ej1.consultar(sql, fabricante);
	}

}
