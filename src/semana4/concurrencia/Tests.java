package semana4.concurrencia;

public class Tests {
	
	public static void main(String[] args) {
		
		
//		Imprimir a = new Imprimir("Tarea A");
//		Imprimir b = new Imprimir("Tarea B");
//		Imprimir c = new Imprimir("Tarea c");
//		
//		a.start();
//		b.start();
//		c.start();
		
		
		TareaImprimir tarea = new TareaImprimir();
		
		Thread t1 = new Thread(tarea, "Tarea A");
		Thread t2 = new Thread(tarea, "Tarea B");
		Thread t3 = new Thread(tarea, "Tarea C");
		
		t1.start();
		t2.start();
		t3.start();
		
		
	}

}
