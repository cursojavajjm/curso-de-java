package semana4.concurrencia;

public class Imprimir extends Thread {
	
	private String dato;
	
	public Imprimir(String dato) {
		this.dato = dato;
	}
	
	@Override
	public void run() {
		for (int i = 0; i < 20; i++) {
			System.out.println("Imprimiendo "+ getName());
		}
	}

}
