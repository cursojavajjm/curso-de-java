package semana4.concurrencia;

public class TareaImprimir implements Runnable {
	
	public TareaImprimir() {
	}

	@Override
	public void run() {
		for (int i = 0; i < 10; i++) {
			System.out.println("Imprimiendo "+ Thread.currentThread().getName());
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
