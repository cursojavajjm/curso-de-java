package semana2.varios;

import java.util.Arrays;
import java.util.Scanner;

public class SieteYMedia {
	
	int[] baraja;
	double jugador;
	double banca;
	int indice;
	
	public SieteYMedia() {
		this.jugador = 0.0;
		this.banca = 0.0;
		this.indice = 0;
		this.baraja = new int[40];
		barajar();
	}
	
	private void barajar() {
		for(int i=0; i<this.baraja.length; i++) {
			this.baraja[i] = (int) (Math.random() * 11);
		}
	}
	
	public void jugar() {
		this.jugador = 0.0;
		this.banca = 0.0;
		Scanner sc = new Scanner(System.in);
		boolean finPartida = false;
		do {
			System.out.println("Asignando cartas");
			darCarta(true);
			darCarta(false);
			mostrarResultado(false);
			System.out.println("�Quieres una carta?");
			String resp = sc.next();
			switch(resp.toLowerCase()) {
			case "si":
				break;
			case "no":
				finPartida = true;
				if (hasGanado())
					System.out.println("�Has ganado!");
				mostrarResultado(true);
				break;
			default:
				System.out.println("");
			}
		} while(!finPartida);
	}
	
	public int darCarta(boolean jugador) {
		int carta = 0;
		if (jugador) {
			carta = this.baraja[indice];
			if (carta > 7)
				 this.jugador += 0.5;
			else this.jugador += carta;
		} else {
			carta = this.baraja[indice];
			if (carta > 7)
				 this.banca += 0.5;
			else this.banca += carta;
		}
		indice++;
		return carta;
	}
	
	private void mostrarResultado(boolean mostrarBanca) {
		System.out.println("Jugador: "+ this.jugador);
		if (mostrarBanca)
			System.out.println("Banca: 	 "+ this.banca);
	}
	
	private boolean hasGanado() {
		if (this.jugador == 7.5)
			return true;
		if (this.jugador < 7.5) {
			if (this.banca > 7.5)
				 return true;
			else if (this.jugador >= this.banca)
				return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return Arrays.toString(this.baraja);
	}

}
