package semana2.varios;

public class CambioMonedas {
	
	private int billete500;
	private int billete200;
	private int billete100;
	private int billete50;
	private int billete20;
	private int billete10;
	private int billete5;
	private int moneda2;
	private int moneda1;
	private int moneda50;
	private int moneda20;
	private int moneda10;
	private int moneda05;
	private int moneda02;
	private int moneda01;
	
	public CambioMonedas() {
		billete500 = 0;
		billete200 = 0;
		billete100 = 0;
		billete50 = 0;
		billete20 = 0;
		billete10 = 0;
		billete5 = 0;
		moneda2 = 0;
		moneda1 = 0;
		moneda50 = 0;
		moneda20 = 0;
		moneda10 = 0;
		moneda05 = 0;
		moneda02 = 0;
		moneda01 = 0;
	}
	
	public void darCambio(double pago) {
		int centimos = (int) (pago*100);
		System.out.println("Centimos: "+ centimos);
		
		this.billete500 = centimos / (500 * 100);
		centimos = centimos % (500 * 100);
		this.billete200 = centimos / (200 * 100);
		centimos = centimos % (200 * 100);
		this.billete100 = centimos / (100 * 100);
		centimos = centimos % (100 * 100);
		this.billete50 = centimos / (50 * 100);
		centimos = centimos % (50 * 100);
		this.billete20 = centimos / (20 * 100);
		centimos = centimos % (20 * 100);
		this.billete10 = centimos / (10 * 100);
		centimos = centimos % (10 * 100);
		this.billete5 = centimos / (5 * 100);
		centimos = centimos % (5 * 100);
		this.moneda2 = centimos / (2 * 100);
		centimos = centimos % (2 * 100);
		this.moneda1 = centimos / (1 * 100);
		centimos = centimos % 100;
		this.moneda50 = centimos / 50;
		centimos = centimos % 50;
		this.moneda20 = centimos / 20;
		centimos = centimos % 20;
		this.moneda10 = centimos / 10;
		centimos = centimos % 10;
		this.moneda05 = centimos / 5;
		centimos = centimos % 5;
		this.moneda02 = centimos / 2;
		centimos = centimos % 2;
		this.moneda01 = centimos / 1;
		centimos = centimos % 1;
	}

	@Override
	public String toString() {
		return "CambioMonedas [billete500=" + billete500 + ", billete200=" + billete200 + ", billete100=" + billete100
				+ ", billete50=" + billete50 + ", billete20=" + billete20 + ", billete10=" + billete10 + ", billete5="
				+ billete5 + ", moneda2=" + moneda2 + ", moneda1=" + moneda1 + ", moneda50=" + moneda50 + ", moneda20="
				+ moneda20 + ", moneda10=" + moneda10 + ", moneda05=" + moneda05 + ", moneda02=" + moneda02
				+ ", moneda01=" + moneda01 + "]";
	}
	
}
