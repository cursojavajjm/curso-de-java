package semana2.biblioteca;

public class Libro extends Obra {

	private String autor;
	private String genero;
	
	public Libro(String isbn, String titulo) {
		super(isbn, titulo);
	}
	
	public Libro(String isbn, String titulo, String autor, String genero) {
		super(isbn, titulo);
		this.autor = autor;
		this.genero = genero;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	@Override
	public String toString() {
		return "Libro ["+ super.toString() +", autor=" + autor + ", genero=" + genero + "]";
	}
	
	
	
}
