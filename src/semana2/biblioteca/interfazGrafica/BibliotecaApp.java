package semana2.biblioteca.interfazGrafica;

import java.awt.Button;
import java.awt.Frame;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

import semana2.biblioteca.Biblioteca;
import semana2.biblioteca.GestionBibliotecaFicheros;
import semana2.biblioteca.IGestionBibliotecas;
import semana2.biblioteca.Obra;

public class BibliotecaApp extends Frame {
	
	private Biblioteca biblioteca;
	
	private Button verObrasBtn;
	private Button verSociosBtn;
	private TextArea texto;
	
	public BibliotecaApp(Biblioteca biblioteca) {
		super();
		setTitle("Biblioteca App");
		setBounds(150, 150, 1200, 600);
		setVisible(true);
		
		this.biblioteca = biblioteca;
		
		addControls();
	}
	
	private void addControls() {
		// ver Obras
		this.verObrasBtn = new Button("VER OBRAS");
		this.verObrasBtn.setBounds(170, 150, 150, 100);
		this.verObrasBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Ver Obras");
				Collection<Obra> obras = BibliotecaApp.this.biblioteca.getObras();
			}
		});
		
		// ver Socios
		this.verSociosBtn = new Button("VER SOCIOS");
		this.verSociosBtn.setBounds(170, 150, 150, 100);
		this.verSociosBtn.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Ver Socios");
			}
		});
	}
	
	public static void main(String[] args) {
		IGestionBibliotecas gestor = new GestionBibliotecaFicheros();
		Biblioteca bib = new Biblioteca(gestor);
//		bib.mostrarObras();
		
		BibliotecaApp app = new BibliotecaApp(bib);
	}
	
	
}
