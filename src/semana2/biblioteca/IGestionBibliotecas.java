package semana2.biblioteca;

import java.util.HashMap;

public interface IGestionBibliotecas {
	
	public static final String FICHERO_OBRAS = "C:\\Users\\NewUser\\git\\curso-de-java\\src\\semana2\\biblioteca\\obras.dat";
	public static final String FICHERO_SOCIOS = "C:\\Users\\NewUser\\git\\curso-de-java\\src\\semana2\\biblioteca\\socios.dat";
	
	HashMap<String, Obra> cargarObras();
	HashMap<Integer, Socio> cargarSocios();
	
	void guardarObras(HashMap<String, Obra> obras);
	void guardarSocios(HashMap<Integer, Socio> socios);

}
