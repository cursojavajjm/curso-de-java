package semana2.biblioteca;

public class Novela extends Libro {

	public Novela(String isbn, String titulo) {
		super(isbn, titulo);
	}
	
	public Novela(String isbn, String titulo, String autor, String genero) {
		super(isbn, titulo);
		setAutor(autor);
		setGenero(genero);
	}

}
