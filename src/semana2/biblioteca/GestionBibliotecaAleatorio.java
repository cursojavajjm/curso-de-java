package semana2.biblioteca;

import java.util.HashMap;

import utilidades.GeneradorDatos;
import utilidades.GeneradorDatos.Tipo;

public class GestionBibliotecaAleatorio implements IGestionBibliotecas {

	@Override
	public HashMap<String, Obra> cargarObras() {
		HashMap<String, Obra> obras = new HashMap<String, Obra>();
		Obra o = null;
		String isbn = "";
		String titulo = "";
		String autorOdirector = "";
		String genero = "";
		for (int i = 0; i < 21; i++) {
			int n = (int)(Math.random() * 4);
			
			// ESTA HECHO TODO CON PELICULA
			titulo = GeneradorDatos.generar(Tipo.PELICULA);
			if ((n % 2) == 0)
				 autorOdirector = GeneradorDatos.generar(Tipo.NOMBRE_FEMENINO) +" "+ GeneradorDatos.generar(Tipo.APELLIDO);
			else autorOdirector = GeneradorDatos.generar(Tipo.NOMBRE_MASCULINO) +" "+ GeneradorDatos.generar(Tipo.APELLIDO);
			genero = GeneradorDatos.generar(Tipo.GENERO_PELICULA);
			isbn = generarIsbn(titulo);
			
			switch (n) {
			case 1:
				o = new Novela(isbn, titulo, autorOdirector, genero);
				break;
			case 2:
				o = new LibroTecnico(isbn, titulo, autorOdirector, genero);
				break;
			case 3:
				o = new Pelicula(isbn, titulo, autorOdirector);
				break;
			case 4:
				o = new Musica(isbn, titulo, autorOdirector);
				break;
			default:
				break;
			}
			if (o != null)
				obras.put(isbn, o);
		}
		return obras;
	}
	
	private String generarIsbn(String titulo) {
		int num = (int) (Math.random() * 10001);
		String isbn = ""+ num;
		if (titulo != null && titulo.length() > 2) 
			isbn += "-"+ titulo.charAt(1) + titulo.charAt(2); 
		return isbn;
	}

	@Override
	public HashMap<Integer, Socio> cargarSocios() {
		HashMap<Integer, Socio> socios = new HashMap<Integer, Socio>();
		Obra o = null;
		String nombre = "";
		String apellido = "";
		for (int i = 0; i < 21; i++) {
			if ((i % 2) == 2)
				 nombre = GeneradorDatos.generar(Tipo.NOMBRE_MASCULINO);
			else nombre = GeneradorDatos.generar(Tipo.NOMBRE_FEMENINO);
			apellido = GeneradorDatos.generar(Tipo.APELLIDO);
			Socio s = new Socio(nombre, apellido);
			socios.put(s.getNumeroSocio(), s);
		}
		return socios;
	}

	@Override
	public void guardarObras(HashMap<String, Obra> obras) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void guardarSocios(HashMap<Integer, Socio> socios) {
		// TODO Auto-generated method stub
		
	}

}
