package semana2.biblioteca;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Obra implements Serializable {
	
	private String isbn;
	private String titulo;
	private List<Prestamo> historialPrestamos;
	private int duracionPrestamo;
	
	public Obra(String isbn, String titulo) {
		this.isbn = isbn;
		this.titulo = titulo;
		this.duracionPrestamo = 14;
		this.historialPrestamos = new ArrayList<Prestamo>();
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public int getDuracionPrestamo() {
		return duracionPrestamo;
	}

	public void setDuracionPrestamo(int duracionPrestamo) {
		if (duracionPrestamo > 0)
			this.duracionPrestamo = duracionPrestamo;
	}
	
	public boolean anadirPrestamo(Prestamo p) {
		return this.historialPrestamos.add(p);
	}

	@Override
	public String toString() {
		return "isbn=" + isbn + ", titulo=" + titulo;
	}
	

}
