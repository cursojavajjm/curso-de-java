package semana2.biblioteca;

public class LibroTecnico extends Libro {
	
	public LibroTecnico(String isbn, String titulo) {
		super(isbn, titulo);
		setDuracionPrestamo(7);
	}
	
	public LibroTecnico(String isbn, String titulo, String autor, String genero) {
		super(isbn, titulo);
		setAutor(autor);
		setGenero(genero);
		setDuracionPrestamo(7);
	}

}
