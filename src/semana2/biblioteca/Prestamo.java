package semana2.biblioteca;

import java.util.Date;

public class Prestamo {
	
	private Date fechaPrestamo;
	private Date fechaDevolucion;
	private int numSocio;
	
	public Prestamo(int numSocio) {
		this.fechaPrestamo = new Date();
		this.numSocio = numSocio;
	}

	public Date getFechaPrestamo() {
		return fechaPrestamo;
	}

	public void setFechaPrestamo(Date fechaPrestamo) {
		this.fechaPrestamo = fechaPrestamo;
	}

	public Date getFechaDevolucion() {
		return fechaDevolucion;
	}

	public void setFechaDevolucion(Date fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}

	public int getNumSocio() {
		return numSocio;
	}

	public void setNumSocio(int numSocio) {
		this.numSocio = numSocio;
	}

	@Override
	public String toString() {
		return "Prestamo [fechaPrestamo=" + fechaPrestamo + ", fechaDevolucion=" + fechaDevolucion + ", numSocio="
				+ numSocio + "]";
	}

}
