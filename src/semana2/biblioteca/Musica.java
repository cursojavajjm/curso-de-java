package semana2.biblioteca;

import java.util.ArrayList;
import java.util.List;

public class Musica extends Obra {
	
	private String autor;
	private List<String> canciones;
	
	public Musica(String isbn, String titulo) {
		super(isbn, titulo);
		this.canciones = new ArrayList<String>();
	}

	public Musica(String isbn, String titulo, String autor) {
		super(isbn, titulo);
		this.autor = autor;
		this.canciones = new ArrayList<String>();
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public List<String> getCanciones() {
		return canciones;
	}

	public void setCanciones(List<String> canciones) {
		this.canciones = canciones;
	}

	@Override
	public String toString() {
		return "Musica ["+ super.toString() +", autor=" + autor + "]";
	}

}
