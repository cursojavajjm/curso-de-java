package semana2.biblioteca;

import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

public class GestionBibliotecaFicheros implements IGestionBibliotecas {

	@Override
	public HashMap<String, Obra> cargarObras() {
		HashMap<String, Obra> obras = new HashMap<String, Obra>();
		FileInputStream fi = null;
		ObjectInputStream entrada = null;
		try {
			fi = new FileInputStream(FICHERO_OBRAS);
			entrada = new ObjectInputStream(fi);
			
			Obra o;
			while (true) {
				o = (Obra) entrada.readObject();
				obras.put(o.getIsbn(), o);
			}
		} catch (EOFException e) {
			return obras;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fi != null)
				try {
					fi.close();
				} catch (Exception e2) { /**/ }
			if (entrada != null)
				try {
					entrada.close();
				} catch (Exception e2) { /**/ }
		}
		return null;
	}

	@Override
	public HashMap<Integer, Socio> cargarSocios() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarObras(HashMap<String, Obra> obras) {
		FileOutputStream fo = null;
		ObjectOutputStream salida = null;
		try {
			fo = new FileOutputStream(FICHERO_OBRAS);
			salida = new ObjectOutputStream(fo);
			
			for(Obra o : obras.values()) {
				salida.writeObject(o);
			}
			System.out.println("Fichero de obras guardado");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fo != null)
				try {
					fo.close();
				} catch (Exception e2) { /**/ }
			if (salida != null)
				try {
					salida.close();
				} catch (Exception e2) { /**/ }
		}
	}

	@Override
	public void guardarSocios(HashMap<Integer, Socio> socios) {
		
	}

}
