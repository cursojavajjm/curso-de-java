package semana2.biblioteca;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

public class Socio implements Serializable {
	
	private static final AtomicInteger numSocio = new AtomicInteger(0); 
	
	public static final long MILISEGUNDOS_AL_DIA = 24 * 60 * 60 * 1000;
	public static final long MILISEGUNDOS_AL_ANYO = MILISEGUNDOS_AL_DIA * 365;
	
	private int numeroSocio;
	private String nombre;
	private String apellido;
	private Date fechaAlta;
	private int prestamosDisponibles;
	
	public Socio(String nombre, String apellido) {
		this.numeroSocio = numSocio.incrementAndGet();
		this.nombre = nombre;
		this.apellido = apellido;
		this.fechaAlta = new Date();
		this.prestamosDisponibles = 2;
	}

	public int getNumeroSocio() {
		return numeroSocio;
	}

	public void setNumeroSocio(int numeroSocio) {
		this.numeroSocio = numeroSocio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	
	public int getPrestamosDisponibles() {
		return prestamosDisponibles;
	}
	
	public void reducirPrestamosDisponibles() {
		if (this.prestamosDisponibles > 0)
			this.prestamosDisponibles++;
	}
	
	public void ampliarPrestamosDisponibles() {
		if (this.prestamosDisponibles < 4)
			this.prestamosDisponibles++;
	}

	public void setPrestamosDisponibles(int prestamosDisponibles) {
		this.prestamosDisponibles = prestamosDisponibles;
	}
	
	public void comprobarAntiguedad() {
		long t1 = (new Date()).getTime();
		long t2 = this.fechaAlta.getTime();
		long dif = (t1-t2)/MILISEGUNDOS_AL_DIA;
		if (dif > 365 && this.prestamosDisponibles < 4)
			this.prestamosDisponibles++;
		
	}

	@Override
	public String toString() {
		return "Socio [numeroSocio=" + numeroSocio + ", nombre=" + nombre + ", apellido=" + apellido + ", fechaAlta="
				+ fechaAlta + "]";
	}

}
