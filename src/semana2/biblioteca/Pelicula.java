package semana2.biblioteca;

public class Pelicula extends Obra {
	
	private String director;

	public Pelicula(String isbn, String titulo) {
		super(isbn, titulo);
		setDuracionPrestamo(5);
	}

	public Pelicula(String isbn, String titulo, String director) {
		super(isbn, titulo);
		this.director = director;
		setDuracionPrestamo(5);
	}
	
	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	@Override
	public String toString() {
		return "Pelicula ["+ super.toString() +", director=" + director + "]";
	}
	
	

}
