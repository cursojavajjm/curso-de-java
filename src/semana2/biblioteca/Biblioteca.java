package semana2.biblioteca;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class Biblioteca {
	
	public static final long MILISEGUNDOS_AL_DIA = 24 * 60 * 60 * 1000;
	public static final long CATORCE_DIAS = MILISEGUNDOS_AL_DIA * 14;
	public static final long SIETE_DIAS = MILISEGUNDOS_AL_DIA * 7;
	public static final long CINCO_DIAS = MILISEGUNDOS_AL_DIA * 5;
	
	private HashMap<Integer, Socio> socios;
	private HashMap<String, Obra> obras;
	private HashMap<String, Prestamo> prestamos;
	
	private IGestionBibliotecas gestor;
	
	public Biblioteca(IGestionBibliotecas gestor) {
		this.gestor = gestor;
		/**
		 * Para la primera vez que ejecuteis la carga de Obras con el gestor de ficheros binarios
		 * tendreis el problema de que no ten�is un fichero inicial para cargar.
		 * 
		 * Para solucionar esto pod�is descomentar las siguientes l�neas solo la primera vez que lo ejecuteis:
		 * 
		 * GestionBibliotecaAleatorio temp = new GestionBibliotecaAleatorio();
		 * this.obras = temp.cargarObras();
		 * gestor.guardarObras(this.obras);
		 */
//		GestionBibliotecaAleatorio temp = new GestionBibliotecaAleatorio();
//		this.obras = temp.cargarObras();
//		gestor.guardarObras(this.obras);
		
		this.obras = gestor.cargarObras();
		this.socios = gestor.cargarSocios();
	}
	
	public Collection<Socio> getSocios() {
		Collection<Socio> set = this.socios.values();
		return set;
	}
	
	public Collection<Obra> getObras() {
		Collection<Obra> set = this.obras.values();
		return set;
	}
	
	public boolean realizarPrestamo(String isbn, int numSocio) {
		if (!this.prestamos.containsKey(isbn)) {
			Socio socio = this.socios.get(numSocio);
			socio.comprobarAntiguedad();
			int prestamosDisponibles = socio.getPrestamosDisponibles();
			if (prestamosDisponibles == 0)
				return false;
			socio.reducirPrestamosDisponibles();
			Prestamo p = new Prestamo(numSocio);
			this.prestamos.put(isbn, p);
		}
		return false;
	}
	
	
	public boolean devolverPrestamo(String isbn) {
		if (this.prestamos.containsKey(isbn)) {
			Prestamo prestamo = this.prestamos.get(isbn);
			 
			// Ampliamos prestamos disponibles
			int numSocio = prestamo.getNumSocio();
			Socio socio = this.socios.get(numSocio);
			socio.ampliarPrestamosDisponibles();
			
			// A�adir al Historial de Prestamos de la obra
			Obra obra = this.obras.get(isbn);
			prestamo.setFechaDevolucion(new Date());
			obra.anadirPrestamo(prestamo);
			
			// Quitar el prestamo de pendientes
			this.prestamos.remove(isbn);
			return true;
		}
		return false;
	}
	
	public String verObrasNoDevueltas() {
		SimpleDateFormat sf = new SimpleDateFormat("dd-MM-yy hh:mm");
		Date hoy = new Date();
		StringBuilder sb = new StringBuilder();
		for(Entry<String, Prestamo> e : this.prestamos.entrySet()) {
			String isbn = e.getKey();
			Prestamo p = e.getValue();
			
			Obra obra = this.obras.get(isbn);
			int duracionPrestamo = obra.getDuracionPrestamo();
			
			// Comprobamos la diferencia entre 
			// la fecha del prestamo y hoy
			Date fechaPrestamo = p.getFechaPrestamo();
			long t1 = hoy.getTime();
			long t2 = fechaPrestamo.getTime();
			long diasTranscurridos = (long) (t1-t2)/MILISEGUNDOS_AL_DIA;
			if (diasTranscurridos > duracionPrestamo) {
				sb.append("--- LIBRO NO DEVUELTO ---");
				sb.append("ISBN: "+ obra.getIsbn());
				sb.append("Fecha prestamo: "+ sf.format(fechaPrestamo));
				sb.append("Socio: "+ p.getNumSocio());
			}
		}
		return sb.toString();
	}
	
	public void mostrarObras() {
		for(Obra o : this.obras.values()) {
			System.out.println(o.toString());
		}
	}
	
	public void mostrarSocios() {
		for(Socio s : this.socios.values()) {
			System.out.println(s.toString());
		}
	}
	
	public static void main(String[] args) {
		IGestionBibliotecas gestor = new GestionBibliotecaFicheros();
		Biblioteca bib = new Biblioteca(gestor);
		bib.mostrarObras();
	}

}
