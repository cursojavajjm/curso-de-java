package semana2.empleados;

public class Comercial extends Empleado {
	
	public static final int COMISION = 1000;
	
	private double comisionPorVenta;
	private int numProyectos;
	
	public Comercial(String nombre, String apellido) {
		super(nombre, apellido);
		this.comisionPorVenta = 0.0;
		this.numProyectos = 0;
	}
	
	public double getComisionPorVenta() {
		return comisionPorVenta;
	}
	public void setComisionPorVenta(double comisionPorVenta) {
		this.comisionPorVenta = comisionPorVenta;
	}
	public int getNumProyectos() {
		return numProyectos;
	}
	public void setNumProyectos(int numProyectos) {
		this.numProyectos = numProyectos;
	}
	
	@Override
	public double calcularSueldo() {
		double sueldoBase =  super.calcularSueldo();
		double sueldo = sueldoBase + this.getNumProyectos() * COMISION;
		return sueldo;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append(", comisionPorVenta = "+ this.comisionPorVenta);
		sb.append(", numProyectos = "+ this.numProyectos);
		return sb.toString();
	}

}
