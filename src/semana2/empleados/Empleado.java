package semana2.empleados;

public class Empleado implements Comparable<Empleado> {
	
	public static final int PLUS_ANTIGUEDAD = 1000;
	
	private String nombre;
	private String apellido;
	private String numeroEmpleado;
	private int antiguedad;
	private double sueldo;
	
	public Empleado() {
	}

	public Empleado(String nombre, String apellido) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.antiguedad = 0;
		this.sueldo = 0.0;
		generarNumeroEmpleado();
	}
	
	private void generarNumeroEmpleado() {
		int num = (int) (Math.random() * 101);
		char c1 = this.nombre.charAt(0);
		char c2 = this.apellido.charAt(0);
		this.numeroEmpleado = String.valueOf(num) + 
				String.valueOf(c1) +
				String.valueOf(c2);
		
	}
	
	public double calcularSueldo() {
		return this.sueldo + this.antiguedad * PLUS_ANTIGUEDAD;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNumeroEmpleado() {
		return numeroEmpleado;
	}
	public void setNumeroEmpleado(String numeroEmpleado) {
		this.numeroEmpleado = numeroEmpleado;
	}
	public int getAntiguedad() {
		return antiguedad;
	}
	public void setAntiguedad(int antiguedad) {
		this.antiguedad = antiguedad;
	}
	public double getSueldo() {
		return sueldo;
	}
	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((apellido == null) ? 0 : apellido.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empleado other = (Empleado) obj;
		if (apellido == null) {
			if (other.apellido != null)
				return false;
		} else if (!apellido.equals(other.apellido))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "nombre=" + nombre + ", apellido=" + apellido + ", numeroEmpleado=" + numeroEmpleado
				+ ", antiguedad=" + antiguedad + ", sueldo=" + sueldo + "";
	}

	@Override
	public int compareTo(Empleado o) {
		if (this.getAntiguedad() < o.getAntiguedad())
			return -1;
		else if (this.getAntiguedad() > o.getAntiguedad())
			return 1;
		else {
			return this.getApellido().compareTo(o.getApellido());
		}
	}
	

}
