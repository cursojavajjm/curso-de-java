package semana2.empleados;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class GestionEmpleadosApp {
	
	private Set<Empleado> plantilla;
	
	public GestionEmpleadosApp() {
		this.plantilla = new HashSet<Empleado>();
		cargarEmpleados();
	}
	
	private void cargarEmpleados() {
		Empleado e = null;
		for(int i=0; i<15; i++) {
			int n = (int) (Math.random() * 4);
			switch (n) {
			case 1:
				e = new Comercial("Nombre" + (int) (Math.random()*101), "Apellido" + (int) (Math.random()*101));
				break;
			case 2:
				e = new Desarrollador("Nombre" + (int) (Math.random()*101), "Apellido" + (int) (Math.random()*101));			
				break;
			case 3:
				e = new Disenador("Nombre" + (int) (Math.random()*101), "Apellido" + (int) (Math.random()*101));
				break;
			default:
				break;
			}
			
			if (e != null)
				plantilla.add(e);
		}
	}
	
	public void mostrar() {
		for(Empleado e : this.plantilla) {
			System.out.println(e.toString());
		}
	}

	public static void main(String[] args) {
		GestionEmpleadosApp app = new GestionEmpleadosApp();
		app.mostrar();
		
		
	}

}
