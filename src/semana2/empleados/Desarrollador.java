package semana2.empleados;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Desarrollador extends Empleado {
	
	public static final int COMISION_JEFE_PROYECTO = 3000;
	
	private boolean jefeProyecto;
	private Set<String> lenguajes;
	
	public Desarrollador(String nombre, String apellido) {
		super(nombre, apellido);
		this.jefeProyecto = false;
		this.lenguajes = new HashSet<String>();
	}

	public boolean isJefeProyecto() {
		return jefeProyecto;
	}

	public void setJefeProyecto(boolean jefeProyecto) {
		this.jefeProyecto = jefeProyecto;
	}

	public Set<String> getLenguajes() {
		return lenguajes;
	}

	public void setLenguajes(Set<String> lenguajes) {
		this.lenguajes = lenguajes;
	}
	
	@Override
	public double calcularSueldo() {
		double sueldo =  super.calcularSueldo();
		if (this.jefeProyecto)
			sueldo += COMISION_JEFE_PROYECTO;
		return sueldo;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append(", jefeProyecto = "+ this.jefeProyecto);
		sb.append(", lenguajes = "+ Arrays.toString(lenguajes.toArray()));
		return sb.toString();
	}

}
