package semana2.empleados;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Disenador extends Empleado {
	
	private Set<String> programas;
	
	public Disenador(String nombre, String apellido) {
		super(nombre, apellido);
		this.programas = new HashSet<String>();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(super.toString());
		sb.append(", programas = "+ Arrays.toString(programas.toArray()));
		return sb.toString();
	}

	public Set<String> getProgramas() {
		return programas;
	}

	public void setProgramas(Set<String> programas) {
		this.programas = programas;
	}
	
}
