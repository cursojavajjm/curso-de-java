package semana2.colegio;

import java.util.ArrayList;
import java.util.List;

public class Profesor extends Persona {
	
	private int antiguedad;
	private List<String> asignaturas;
	
	public Profesor() {
		super();
		System.out.println("Nombre: "+ this.getNombre());
	}
	
	public Profesor(String nombre, String dni) {
		super(nombre, dni);
	}

	public int getAntiguedad() {
		return antiguedad;
	}

	public void setAntiguedad(int antiguedad) {
		this.antiguedad = antiguedad;
	}

//	public ArrayList<String> getAsignaturas() {
//		return asignaturas;
//	}

	public void setAsignaturas(ArrayList<String> asignaturas) {
		this.asignaturas = asignaturas;
	}
	

}
