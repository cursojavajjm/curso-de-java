package semana2.colegio;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Colegio {
	
	public static void verAsignaturas(Profesor prof) {
		// algunas operaciones con prof
	}
	
	public static void tratarListas(List<?> lista) {
		// algunas operaciones con prof
	}

	public static void main(String[] args) {
		Profesor prof;
		
		prof = new ProfesorAsistente("Juan", "38120123Q");
		verAsignaturas(prof);
		
		prof= new ProfesorInterino("Juan", "38120123Q");
		verAsignaturas(prof);
		
		LinkedList<String> lista = new LinkedList<String>();
		
//		ArrayList<String> lista = new ArrayList<String>();
//		tratarListas(lista);
	}

}
