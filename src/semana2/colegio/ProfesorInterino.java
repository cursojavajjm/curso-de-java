package semana2.colegio;

import java.util.Date;

public class ProfesorInterino extends Profesor {

	private Date fechaInicio;
	private Date fechaFin;
	
	public ProfesorInterino() {
		super();
	}
	
	public ProfesorInterino(String nombre, String dni) {
		super(nombre, dni);
		this.setAntiguedad(0);
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
}
