package semana2.colegio;

public class ProfesorAsistente extends Profesor {
	
	public ProfesorAsistente() {
		super();
	}
	
	public ProfesorAsistente(String nombre, String dni) {
		super(nombre, dni);
		this.setAntiguedad(0);
	}

}
